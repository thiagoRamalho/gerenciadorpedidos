package br.com.trama.gerenciadorpedidos.views.list.produtos;

import java.util.ArrayList;
import java.util.List;

import br.com.trama.gerenciadorpedidos.model.dao.ProdutoDAO;
import br.com.trama.gerenciadorpedidos.model.entity.Fornecedor;
import br.com.trama.gerenciadorpedidos.model.entity.Produto;
import br.com.trama.gerenciadorpedidos.views.list.SearchStrategy;

public class SearchFornecedor implements SearchStrategy<Produto>{

	private ProdutoDAO produtoDAO;
	private Fornecedor fornecedor;
	
	public SearchFornecedor(ProdutoDAO produtoDAO, Fornecedor fornecedor) {
		super();
		this.produtoDAO = produtoDAO;
		this.fornecedor = fornecedor;
	}

	@Override
	public List<Produto> search() {
		
		List<Produto> produtos = new ArrayList<Produto>();
		
		if(this.produtoDAO != null && fornecedor != null){
			produtos = this.produtoDAO.findByFornecedor(fornecedor.getId());
		}
		
		return produtos;
	}

	@Override
	public List<Produto> searchBy(String text) {
		return this.produtoDAO.findByNomeAndFornecedor(text, true, fornecedor.getId());
	}

}
