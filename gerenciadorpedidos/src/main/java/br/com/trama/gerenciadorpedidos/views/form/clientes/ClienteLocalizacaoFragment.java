package br.com.trama.gerenciadorpedidos.views.form.clientes;

import roboguice.inject.InjectResource;
import roboguice.inject.InjectView;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import br.com.trama.gerenciadorpedidos.R;
import br.com.trama.gerenciadorpedidos.model.entity.Endereco;
import br.com.trama.gerenciadorpedidos.util.ConstantsUtil;
import br.com.trama.maskinput.MaskWatcher;

public class ClienteLocalizacaoFragment extends ClienteFormTemplate{

	private final String TAG = ClienteLocalizacaoFragment.class.getSimpleName();
	
	@InjectView(R.id.logradouro_edit) 
	private EditText logradouro;

	@InjectView(R.id.numero_edit) 
	private EditText numero;

	@InjectView(R.id.cep_edit) 
	private EditText cep;

	@InjectView(R.id.complemento_edit) 
	private EditText complemento;

	@InjectView(R.id.bairro_edit) 
	private EditText bairro;

	@InjectView(R.id.cidade_edit) 
	private EditText cidade;

	@InjectView(R.id.uf_id)
	private Spinner uf;
 
	@InjectResource(R.array.option_uf)
	private String[] option_uf;

	private ArrayAdapter<String> ufAdapter;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		setRetainInstance(true);
		
		return inflater.inflate(R.layout.cliente_localizacao_form_layout, container, false);
	}
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {

		super.onViewCreated(view, savedInstanceState);
		
		configComponents();
		
		populateForm(savedInstanceState);
	}

	private void populateForm(Bundle savedInstanceState) {
		
		Log.d(TAG, "populateForm");
		
		getEntity(savedInstanceState);

		Endereco endereco = super.entity.getEndereco();
		
		logradouro.setText(endereco.getLogradouro());
		
		numero.setText(endereco.getNumero());
		
		cep.setText(endereco.getCep());
		
		bairro.setText(endereco.getBairro());
		
		cidade.setText(endereco.getCidade());
		
		int position = ufAdapter.getPosition(endereco.getUf());
		
		uf.setSelection(position);
		
		complemento.setText(endereco.getComplemento());
	}

	private void configComponents() {
		
		cep.addTextChangedListener(new MaskWatcher(ConstantsUtil.MASK_CEP, ConstantsUtil.DENIEL_NUMBERS_REGEX));
		cep.setRawInputType(InputType.TYPE_CLASS_NUMBER);
		ufAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, option_uf);
		uf.setAdapter(ufAdapter);
	}

	protected void populateEntity() {
		
		Log.d(TAG, "populateEntity");
		
		Endereco endereco = super.entity.getEndereco();
		
		endereco.setBairro(super.componentUtil.extractValue(bairro));

		endereco.setCep(super.componentUtil.extractValue(cep));
		
		endereco.setCidade(super.componentUtil.extractValue(cidade));
		
		endereco.setComplemento(super.componentUtil.extractValue(complemento));
		
		endereco.setLogradouro(super.componentUtil.extractValue(logradouro));
		
		endereco.setNumero(super.componentUtil.extractValue(numero));
		
		endereco.setUf(super.componentUtil.extractValue(uf));
		
		super.entity.setEndereco(endereco);
	}

	protected boolean validate() {
		
		boolean isOk = true;
		
		if(super.componentUtil.isEmpty(bairro)){
			bairro.setError(super.invalidValue);
			isOk = false;
		}

		if(!super.componentUtil.containsMin(cep, ConstantsUtil.SIZE_8, ConstantsUtil.DENIEL_NUMBERS_REGEX)){
			cep.setError(super.invalidValue);
			isOk = false;
		}
		
		if(super.componentUtil.isEmpty(cidade)){
			cidade.setError(super.invalidValue);
			isOk = false;
		}

		if(super.componentUtil.isEmpty(logradouro)){
			logradouro.setError(super.invalidValue);
			isOk = false;
		}
		
		if(super.componentUtil.isEmpty(numero)){
			numero.setError(super.invalidValue);
			isOk = false;
		}
		
		return isOk;
	}
}
