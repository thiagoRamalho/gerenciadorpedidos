package br.com.trama.gerenciadorpedidos.views.list.produtos;

import java.util.List;

import br.com.trama.gerenciadorpedidos.model.dao.ProdutoDAO;
import br.com.trama.gerenciadorpedidos.model.entity.Produto;
import br.com.trama.gerenciadorpedidos.views.list.SearchStrategy;

class SearchAll implements SearchStrategy<Produto>{

	private ProdutoDAO produtoDAO;
	
	public SearchAll(ProdutoDAO produtoDAO) {
		super();
		this.produtoDAO = produtoDAO;
	}

	@Override
	public List<Produto> search() {
		return produtoDAO.findAll();
	}

	@Override
	public List<Produto> searchBy(String text) {
		return produtoDAO.findByNome(text);
	}
}
