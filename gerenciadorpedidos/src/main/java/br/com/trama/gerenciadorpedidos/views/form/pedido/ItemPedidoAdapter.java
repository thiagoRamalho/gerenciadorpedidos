package br.com.trama.gerenciadorpedidos.views.form.pedido;

import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import br.com.trama.gerenciadorpedidos.R;
import br.com.trama.gerenciadorpedidos.model.entity.ItemPedido;
import br.com.trama.gerenciadorpedidos.util.ComponentUtil;

public class ItemPedidoAdapter extends BaseAdapter{

	private List<ItemPedido> itens;
	private LayoutInflater inflater;
	private ComponentUtil componentUtil;
	
	public ItemPedidoAdapter(List<ItemPedido> itens, Context context) {
		this.itens = itens;
		this.inflater = LayoutInflater.from(context);
		this.componentUtil = new ComponentUtil();
	}
	
	@Override
	public int getCount() {
		return itens.size();
	}

	@Override
	public ItemPedido getItem(int position) {
		return itens.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@SuppressLint("ViewTag")
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		
		TextView nomeItem,  precoUnidade, subTotal, quantidade;
		
		if (convertView == null) {
			
			convertView = inflater.inflate(R.layout.row_item_pedido_layout, parent, false);
			
			nomeItem     = (TextView)  convertView.findViewById(R.id.pedido_item_nome_id);
			subTotal     = (TextView)  convertView.findViewById(R.id.pedido_item_sub_total_id);
			quantidade   = (TextView)  convertView.findViewById(R.id.pedido_quantidade_item_id);
			precoUnidade = (TextView)  convertView.findViewById(R.id.pedido_preco_unidade_id);
			
			convertView.setTag(R.id.pedido_item_nome_id, nomeItem);
			convertView.setTag(R.id.produto_preco_vista_id, subTotal);
			convertView.setTag(R.id.pedido_quantidade_item_id, quantidade);
			convertView.setTag(R.id.pedido_preco_unidade_id,   precoUnidade);
		
		} else {
			nomeItem     = (TextView)  convertView.getTag(R.id.pedido_item_nome_id);
			subTotal     = (TextView)  convertView.getTag(R.id.produto_preco_vista_id);
			quantidade   = (TextView)  convertView.getTag(R.id.pedido_quantidade_item_id);
			precoUnidade = (TextView)  convertView.getTag(R.id.pedido_preco_unidade_id);
		}
		
		ItemPedido item = getItem(position);
		
		nomeItem.setText(item.getNome());
		
		precoUnidade.setText(this.componentUtil.doubleToCurrency(item.getPreco()));
		
		quantidade.setText(""+item.getQuantidade());
		
		subTotal.setText(this.componentUtil.doubleToCurrency(item.calcularTotal()));
		
		return convertView;
	}
}
