package br.com.trama.gerenciadorpedidos.views.list.fornecedor;

import java.util.List;

import br.com.trama.gerenciadorpedidos.model.dao.FornecedorDAO;
import br.com.trama.gerenciadorpedidos.model.entity.Fornecedor;
import br.com.trama.gerenciadorpedidos.views.list.SearchStrategy;

public class SearchAtivo implements SearchStrategy<Fornecedor>{

	private FornecedorDAO fornecedorDAO;
	
	public SearchAtivo(FornecedorDAO fornecedorDAO) {
		super();
		this.fornecedorDAO = fornecedorDAO;
	}

	@Override
	public List<Fornecedor> search() {
		return this.fornecedorDAO.findByAtivo(true);
	}

	@Override
	public List<Fornecedor> searchBy(String text) {
		return this.fornecedorDAO.findByNomeAndAtivo(text, true);
	}
}
