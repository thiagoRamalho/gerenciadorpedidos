package br.com.trama.gerenciadorpedidos.views.form.settings;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import br.com.trama.gerenciadorpedidos.R;

public class Settings {

	private SharedPreferences sharedPref;
	private Context context;

	public Settings(Context context){
		this.context = context;
		sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
	}
	
	public boolean isSendEmail(){
		return sharedPref.getBoolean(context.getString(R.string.pref_send_order_email_key), false);
	}

	public boolean isSendEmailFornecedor(){
		return sharedPref.getBoolean(context.getString(R.string.pref_send_mail_for_key), false);
	}

	public boolean isSendEmailCliente(){
		return sharedPref.getBoolean(context.getString(R.string.pref_send_mail_cli_key), false);
	}

	public int getPort() {
		
		String string = context.getString(R.string.pref_smtp_port_key);
		
		String strInt = sharedPref.getString(string, "-1");
		
		return Integer.valueOf(strInt);
	}

	public String getSMTP() {
		return sharedPref.getString(context.getString(R.string.pref_smtp_address_key), "");
	}
	
	public String getUserSMTP() {
		return sharedPref.getString(context.getString(R.string.pref_smtp_user_key), "");
	}
	
	public String getUserPassSMTP() {
		return sharedPref.getString(context.getString(R.string.pref_smtp_pass_key), "");
	}

	public String getVendedor() {
		return sharedPref.getString(context.getString(R.string.pref_nome_apresentacao_vendedor_key), "");
	}
}
