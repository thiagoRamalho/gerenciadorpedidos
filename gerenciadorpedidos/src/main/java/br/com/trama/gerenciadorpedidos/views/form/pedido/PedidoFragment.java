package br.com.trama.gerenciadorpedidos.views.form.pedido;

import java.util.Calendar;
import java.util.List;

import roboguice.inject.InjectView;
import android.app.Activity;
import android.app.AlertDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TabHost;
import android.widget.TabHost.TabSpec;
import android.widget.TextView;
import br.com.trama.gerenciadorpedidos.MainActivity;
import br.com.trama.gerenciadorpedidos.R;
import br.com.trama.gerenciadorpedidos.listeners.CancelClickListener;
import br.com.trama.gerenciadorpedidos.listeners.ItemPedidoListClickListener;
import br.com.trama.gerenciadorpedidos.mail.SendMailIntentService;
import br.com.trama.gerenciadorpedidos.model.dao.ProdutoDAO;
import br.com.trama.gerenciadorpedidos.model.entity.Carrinho;
import br.com.trama.gerenciadorpedidos.model.entity.Cliente;
import br.com.trama.gerenciadorpedidos.model.entity.Fornecedor;
import br.com.trama.gerenciadorpedidos.model.entity.ItemPedido;
import br.com.trama.gerenciadorpedidos.model.entity.Pedido;
import br.com.trama.gerenciadorpedidos.model.entity.Produto;
import br.com.trama.gerenciadorpedidos.util.ComponentUtil;
import br.com.trama.gerenciadorpedidos.util.RouterUtil;

import com.actionbarsherlock.app.ActionBar;
import com.github.rtyley.android.sherlock.roboguice.fragment.RoboSherlockFragment;
import com.google.inject.Inject;

public class PedidoFragment extends RoboSherlockFragment{

	private final String TAG = PedidoFragment.class.getSimpleName();

	@InjectView(R.id.tabhost)
	private TabHost tabHost;

	@InjectView(R.id.itens_btn_limpar)
	private ImageButton limpar;

	@InjectView(R.id.itens_btn_salvar)
	private ImageButton salvar;

	@InjectView(R.id.itens_btn_duplicar)
	private ImageButton duplicar;

	@InjectView(R.id.btn_selecao_cliente_id)
	private Button selecaoCliente;

	@InjectView(R.id.btn_selecao_fornecedor_id)
	private Button selecaoFornecedor;

	@InjectView(R.id.pedido_observacoes_id)
	private  EditText observacoes;

	@InjectView(R.id.btn_selecao_produto_id)
	private Button selecaoProdutos;

	@InjectView(R.id.pedido_total_id)
	private TextView total;

	@InjectView(R.id.lista_itens_id)
	private ListView listViewItens;

	@InjectView(R.id.radioPanel)
	private RadioGroup condicoesPagamento;

	@Inject
	private RouterUtil routerUtil;

	@Inject
	private ComponentUtil componentUtil;
	
	@Inject
	private ProdutoDAO produtoDAO;

	private AlertDialog dialogValidateSelection;

	private CallBackPedido callBackPedido;

	private ActionBar actionBar;

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {

		super.onViewCreated(view, savedInstanceState);

		Log.d(TAG, "onViewCreated");

		configTabs();

		updateDataComponent();

		configComponents();

		configTitle();
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		prepareReload();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		Log.d(TAG, "onCreateView");

		actionBar = getSherlockActivity().getSupportActionBar();

		return inflater.inflate(R.layout.pedido_container_fragment, container, false);
	}

	private void configTitle() {
		String title = getString(R.string.novo_pedido);

		Long id = Carrinho.getPedido().getId();

		if(id != null){
			title = getString(R.string.visualizar_pedido, Carrinho.getPedido().getId());
		}

		actionBar.setTitle(title);
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);

		// This makes sure that the container activity has implemented
		// the callback interface. If not, it throws an exception
		try {
			this.callBackPedido = (CallBackPedido) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.getClass().getCanonicalName() + " must implement CallBackPedido");
		}	
	}


	@Override
	public void onPause() {
		Log.d(TAG, "onPause");
		super.onPause();
	}

	private void configComponents() {

		Log.d(TAG, "configComponents");

		Long id = Carrinho.getPedido().getId();

		boolean isEdit = id != null;

		observacoes.setEnabled(!isEdit);

		selecaoCliente.setOnClickListener(new ClienteClickListener());
		selecaoCliente.setEnabled(!isEdit);

		selecaoFornecedor.setOnClickListener(new FornecedorClickListener());
		selecaoFornecedor.setEnabled(!isEdit);

		selecaoProdutos.setOnClickListener(new AdicionarItensClickListener());
		selecaoProdutos.setEnabled(Carrinho.getPedido().getFornecedor() != null);

		if(selecaoProdutos.isEnabled()){
			selecaoProdutos.setEnabled(!isEdit);
		}

		listViewItens.setEnabled(!isEdit);
		
		if(!isEdit){
			listViewItens.setOnItemClickListener(new ItemPedidoListClickListener(this));
		}

		int childCount = condicoesPagamento.getChildCount();
		
		for (int index = 0; index < childCount; index++) {
			condicoesPagamento.getChildAt(index).setEnabled(!isEdit);
		}
		
		condicoesPagamento.setOnCheckedChangeListener(new ClickRadioButton());

		AlertDialog.Builder builder = new AlertDialog.Builder(this.getActivity());

		builder.setNeutralButton(android.R.string.ok, new CancelClickListener());

		builder.setTitle(R.string.atencao);

		dialogValidateSelection = builder.create();

		duplicar.setOnClickListener(new DuplicarClickListener());
		duplicar.setEnabled(isEdit);

		limpar.setOnClickListener(new LimparClickListener());
		limpar.setEnabled(!isEdit);

		salvar.setOnClickListener(new SalvarClickListener());
		salvar.setEnabled(!isEdit);
		
		total.setEnabled(!isEdit);
	}

	private void updateDataComponent() {

		Log.d(TAG, "updateDataComponent");

		Pedido pedido = Carrinho.getPedido();

		Cliente cliente = pedido.getCliente();

		Fornecedor fornecedor = pedido.getFornecedor();

		selecaoCliente.setText("");

		if(cliente != null){
			selecaoCliente.setText(cliente.getNomeFantasia());
		}

		selecaoFornecedor.setText("");

		if(fornecedor != null){
			selecaoFornecedor.setText(fornecedor.getNome());
		}

		observacoes.setText(pedido.getObservacoes());

		int id = pedido.isAVista() ? R.id.pedido_radio_vista_edit : R.id.pedido_radio_prazo_edit;
		condicoesPagamento.check(id);

		updateItens();
	}

	public void updateItens() {

		Pedido pedido = Carrinho.getPedido();
		
		ItemPedidoAdapter itemPedidoAdapter = new ItemPedidoAdapter(pedido.getItens(), this.getActivity());

		listViewItens.setAdapter(itemPedidoAdapter);

		total.setText(componentUtil.doubleToCurrency(pedido.getTotal()));
	}

	private void configTabs() {

		Log.d(TAG, "configTabs");

		tabHost.setup();

		TabSpec tabPedido = tabHost.newTabSpec("tabPedido");
		tabPedido.setIndicator(getString(R.string.clientes_txt));
		tabPedido.setContent(R.id.cli_forn_pedido_id);
		tabHost.addTab(tabPedido);

		TabSpec tabProdutos = tabHost.newTabSpec("tabProdutos");
		tabProdutos.setIndicator(getString(R.string.produtos_txt));
		tabProdutos.setContent(R.id.itens_pedido_id);
		tabHost.addTab(tabProdutos);

		tabHost.setCurrentTab(Carrinho.getCurrentTab());
	}

	private void prepareReload() {
		Pedido pedido = Carrinho.getPedido();
		pedido.setObservacoes(observacoes.getText().toString());
		pedido.setAVista(isSelectedAVista());
		Carrinho.setCurrentTab(tabHost.getCurrentTab());
	}

	private boolean isSelectedAVista() {
		int checkedRadioButtonId = condicoesPagamento.getCheckedRadioButtonId();
		return checkedRadioButtonId == R.id.pedido_radio_vista_edit;
	}

	private boolean validate() {

		boolean isOk = true;

		int rIdMsg = 0;
		int tabIndex = 0;

		Pedido pedido = Carrinho.getPedido();

		Cliente cliente = pedido.getCliente();

		if(cliente == null){
			rIdMsg = R.string.msg_selecione_cliente;
			tabIndex = 0;
			isOk = false;
		}
		else if(pedido.getFornecedor() == null){
			rIdMsg = R.string.msg_selecione_fornecedor;
			tabIndex = 0;
			isOk = false;
		}
		else if(pedido.getItens().isEmpty()){
			rIdMsg = R.string.msg_selecione_produtos;
			tabIndex = 1;
			isOk = false;
		}

		if(!isOk){
			dialogValidateSelection.setMessage(getString(rIdMsg));
			dialogValidateSelection.show();
			tabHost.setCurrentTab(tabIndex);
		}

		return isOk;
	}

	private void rebindItens() {
		
		Pedido novoPedido = Carrinho.getPedido();
		
		//recupera a lista de itens para verificar 
		//possiveis alteracoes de preco
		List<ItemPedido> itens = novoPedido.getItens();
		
		for (ItemPedido itemPedido : itens) {
			
			Produto prod = produtoDAO.findById(itemPedido.getProdutoId());
			
			itemPedido.setNome(prod.getNome());
			itemPedido.setPreco(novoPedido.isAVista() ? prod.getPrecoUnidadeVista() : prod.getPrecoUnidadePrazo());
		}
	}

	private class ClienteClickListener implements OnClickListener{

		@Override
		public void onClick(View v) {
			prepareReload();
			callBackPedido.adicionarClienteAoCarrinho();
		}
	}

	private class FornecedorClickListener implements OnClickListener{

		@Override
		public void onClick(View v) {
			prepareReload();
			callBackPedido.adicionarFornecedorAoCarrinho();
		}
	}

	private class AdicionarItensClickListener implements OnClickListener{

		@Override
		public void onClick(View v) {
			prepareReload();
			callBackPedido.adicionarItemAoCarrinho();
		}
	}

	private class LimparClickListener implements OnClickListener{

		@Override
		public void onClick(View v) {
			Carrinho.clean();
			updateDataComponent();
			configComponents();
			Carrinho.setCurrentTab(0);
			tabHost.setCurrentTab(0);
		}
	}


	private class SalvarClickListener implements OnClickListener{

		@Override
		public void onClick(View v) {

			if(validate()){

				salvar.setEnabled(false);
				limpar.setEnabled(false);
				duplicar.setEnabled(false);

				prepareReload();
				Pedido pedido = Carrinho.getPedido();
				pedido.setDate(Calendar.getInstance());
				pedido.getTotal();
				pedido.setSincronizado(false);
				pedido.save();
				ItemPedido.saveInTx(pedido.getItens());

				Carrinho.clean();

				routerUtil
				.withContext(getActivity())
				.withService(SendMailIntentService.class)
				.startService();

				routerUtil
				.withContext(getActivity())
				.withClass(MainActivity.class)
				.route();

				getActivity().finish();
			}
		}
	}

	private class DuplicarClickListener implements OnClickListener{

		@Override
		public void onClick(View v) {
			prepareReload();
			Pedido origem = Carrinho.getPedido();
			Carrinho.clean();

			Pedido novoPedido = origem.cloneDeep();
			Carrinho.setPedido(novoPedido);

			rebindItens();

			updateDataComponent();
			configComponents();
			configTitle();
		}
	}

	private class ClickRadioButton implements OnCheckedChangeListener{

		@Override
		public void onCheckedChanged(RadioGroup group, int checkedId) {
			Log.d(TAG, "change condicoes pagamento");

			boolean selectedAVista = isSelectedAVista();
			Carrinho.getPedido().setAVista(selectedAVista);

			rebindItens();
			updateItens();
		}
	}
}
