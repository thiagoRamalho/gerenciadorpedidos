package br.com.trama.gerenciadorpedidos.mail;

import java.io.File;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.net.smtp.AuthenticatingSMTPClient;
import org.apache.commons.net.smtp.SMTPClient;
import org.apache.commons.net.smtp.SMTPReply;
import org.apache.commons.net.smtp.SimpleSMTPHeader;

public class SendMailCommonsNetBuilder implements SendMailBuilder {

	private final String hostName;
	private final int port;
	private String user;
	private String password;
	private String from;
	private List<String> recipients;
	private String subject;
	private String messageBody;
	private String returnMessage;
	private AuthenticatingSMTPClient authenticatingSMTPClient;
	private String to;

	public SendMailCommonsNetBuilder(String hostName, int port) {
		super();
		this.hostName = hostName;
		this.port = port;
		this.recipients = new ArrayList<String>();
	}

	/* (non-Javadoc)
	 * @see br.com.trama.gerenciadorpedidos.mail.SendMailBuilder#auth(java.lang.String, java.lang.String)
	 */
	@Override
	public SendMailBuilder auth(String user, String password){
		this.user = user;
		this.password = password;

		return this;
	}

	/* (non-Javadoc)
	 * @see br.com.trama.gerenciadorpedidos.mail.SendMailBuilder#from(java.lang.String)
	 */
	@Override
	public SendMailBuilder from(String from){
		this.from = from;
		return this;
	}

	/* (non-Javadoc)
	 * @see br.com.trama.gerenciadorpedidos.mail.SendMailBuilder#to(java.lang.String)
	 */
	@Override
	public SendMailBuilder to(String to){
		this.to = to;
		return this;
	}

	/* (non-Javadoc)
	 * @see br.com.trama.gerenciadorpedidos.mail.SendMailBuilder#cc(java.lang.String)
	 */
	@Override
	public SendMailBuilder cc(String cc){
		recipients.add(cc);
		return this;
	}

	/* (non-Javadoc)
	 * @see br.com.trama.gerenciadorpedidos.mail.SendMailBuilder#subject(java.lang.String)
	 */
	@Override
	public SendMailCommonsNetBuilder subject(String subject){
		this.subject = subject;
		return this;
	}

	public SendMailBuilder withMessageBody(String messageBody){
		this.messageBody = messageBody;
		return this;
	}

	public String returnMessage(){
		return this.returnMessage;
	}
	
	/* (non-Javadoc)
	 * @see br.com.trama.gerenciadorpedidos.mail.SendMailBuilder#send()
	 */
	@Override
	public boolean send(){
		boolean isSend = false;

		try{

			authenticatingSMTPClient = new AuthenticatingSMTPClient();

			connect();

			addFromTo();

			addMessage();

			if(!authenticatingSMTPClient.completePendingCommand()){
				runException("completePendingCommand");
			}

			isSend = true;

		} catch(Exception e){
			this.returnMessage = ""+e.getMessage();
		}
		finally{
			this.finish();
		}

		return isSend;
	}

	private void addMessage() throws IOException {

		Writer sendMessageData = authenticatingSMTPClient.sendMessageData();

		
		if(sendMessageData == null){
			runException("sendMessageData");
		}

		SimpleSMTPHeader header = new SimpleSMTPHeader(from, to, subject);
		sendMessageData.write(header.toString());
		sendMessageData.write(this.messageBody);
		sendMessageData.close();
	}

	private void addFromTo() throws IOException {

		authenticatingSMTPClient.setSender(from);
		checkReply(authenticatingSMTPClient);

		authenticatingSMTPClient.addRecipient(to);
		checkReply(authenticatingSMTPClient);

		for (String cc: this.recipients) {
			authenticatingSMTPClient.addRecipient(cc);
		}
		checkReply(authenticatingSMTPClient);
	}

	private void connect(){ 

		try{
			authenticatingSMTPClient.connect(hostName, port);

			authenticatingSMTPClient.ehlo(""+System.currentTimeMillis());

			if(!authenticatingSMTPClient.execTLS()){
				runException("STARTTLS was not accepted");
			}

			authenticatingSMTPClient.auth(AuthenticatingSMTPClient.AUTH_METHOD.LOGIN, user, password);
			checkReply(authenticatingSMTPClient);

		}catch(Exception e){
			throw new RuntimeException("connect | " + e.getMessage());
		}
	}

	private void runException(String msg) {


		StringBuilder builder = new StringBuilder(msg.trim());

		try{

			builder
			.append(" | ")
			.append("Failure to send the email ")
			.append(""+authenticatingSMTPClient.getReply())
			.append(" | ")
			.append(""+authenticatingSMTPClient.getReplyString());

		}catch(IOException e){
			throw new RuntimeException(e.getMessage());
		}

		throw new RuntimeException(builder.toString());
	}

	private void checkReply(SMTPClient sc) throws IOException  {

		if (SMTPReply.isNegativeTransient(sc.getReplyCode())) {
			throw new RuntimeException("Transient SMTP error " + sc.getReply() + sc.getReplyString());
		} 
		else if (SMTPReply.isNegativePermanent(sc.getReplyCode())) {
			throw new RuntimeException("Permanent SMTP error " + sc.getReply() + sc.getReplyString());
		}
	}

	private void finish() {

		if(this.authenticatingSMTPClient != null){

			try {
				if(this.authenticatingSMTPClient.isConnected()){
					this.authenticatingSMTPClient.logout();
					this.authenticatingSMTPClient.disconnect();
				}

			} catch (Exception e) {
				this.returnMessage = e.getMessage();
			}
		}
	}

	@Override
	public SendMailBuilder withAttach(File file) {
		return this;
	}
}
