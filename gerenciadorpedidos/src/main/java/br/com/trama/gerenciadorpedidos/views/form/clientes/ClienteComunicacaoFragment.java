package br.com.trama.gerenciadorpedidos.views.form.clientes;

import roboguice.inject.InjectView;
import android.os.Bundle;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import br.com.trama.gerenciadorpedidos.R;
import br.com.trama.gerenciadorpedidos.model.entity.Comunicacao;
import br.com.trama.gerenciadorpedidos.util.ConstantsUtil;
import br.com.trama.maskinput.MaskWatcher;

public class ClienteComunicacaoFragment extends ClienteFormTemplate{

	private final String TAG = ClienteComunicacaoFragment.class.getSimpleName();
	
	@InjectView(R.id.contato_edit)
	private EditText contato;

	@InjectView(R.id.telefone_edit)
	private EditText telefone;

	@InjectView(R.id.celular_edit)
	private EditText celular;

	@InjectView(R.id.nextel_edit)
	private EditText nextel;

	@InjectView(R.id.email_edit)
	private EditText email;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		return inflater.inflate(R.layout.cliente_comunicacao_form_layout, container, false);
	}
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		
		super.onViewCreated(view, savedInstanceState);
		
		Log.d(TAG, "onViewCreated");
		
		configComponents();
		
		populateForm(savedInstanceState);
	}

	private void populateForm(Bundle savedInstanceState) {
		
		Log.d(TAG, "populateForm");
		
		getEntity(savedInstanceState);
		
		Comunicacao comunicacao = super.entity.getComunicacao();
		
		contato.setText(comunicacao.getContato());
		
		celular.setText(extractTelValue(comunicacao.getCelular()));
		
		telefone.setText(extractTelValue(comunicacao.getTelefone()));
		
		nextel.setText(comunicacao.getNextel());
		
		email.setText(comunicacao.getEmail());
	}


	private String extractTelValue(long value) {
		
		return value < ConstantsUtil.SIZE_8 ? "" : String.valueOf(value);
	}

	
	private void configComponents() {
		
		Log.d(TAG, "configComponents");
		
		telefone.addTextChangedListener(new MaskWatcher(ConstantsUtil.MASK_TEL, ConstantsUtil.DENIEL_NUMBERS_REGEX));
		telefone.setRawInputType(InputType.TYPE_CLASS_NUMBER);
		
		celular.addTextChangedListener  (new MaskWatcher(ConstantsUtil.MASK_CEL, ConstantsUtil.DENIEL_NUMBERS_REGEX));
		celular.setRawInputType(InputType.TYPE_CLASS_NUMBER);

		nextel.setRawInputType(InputType.TYPE_CLASS_NUMBER);
	}

	@Override
	protected boolean validate() {
		
		boolean isOk = true;
		
		if(super.componentUtil.isEmpty(contato)){
			contato.setError(super.invalidValue);
			isOk = false;
		}

		if(!super.componentUtil.validatePhone(telefone, true, ConstantsUtil.SIZE_10, ConstantsUtil.DENIEL_NUMBERS_REGEX)){
			telefone.setError(super.invalidValue);
			isOk = false;
		}
		
		Log.d(TAG, celular.getText().toString());
		
		if(!super.componentUtil.validateMobilePhone(celular, false)){
			celular.setError(super.invalidValue);
			isOk = false;
		}
		
		if(!super.componentUtil.isEmpty(nextel)){
			//email.setError(super.invalidValue);
			//isOk = false;
		}
		
		if(!super.componentUtil.validateEmail(email, false)){
			email.setError(super.invalidValue);
			isOk = false;
		}
		
		return isOk;
	}


	@Override
	protected void populateEntity() {
		
		Log.d(TAG, "populateEntity");
		
		Comunicacao comunicacao = super.entity.getComunicacao();
		
		String extractCel = super.componentUtil.extractValue(celular, ConstantsUtil.DENIEL_NUMBERS_REGEX);
		
		if(!TextUtils.isEmpty(extractCel)){
			comunicacao.setCelular(Long.valueOf(extractCel));
		}
		
		String extractTel = super.componentUtil.extractValue(telefone, ConstantsUtil.DENIEL_NUMBERS_REGEX);
		comunicacao.setTelefone(Long.valueOf(extractTel));
		
		comunicacao.setNextel(super.componentUtil.extractValue(nextel));

		comunicacao.setContato(super.componentUtil.extractValue(contato));
		
		comunicacao.setEmail(super.componentUtil.extractValue(email));
		
		super.entity.setComunicacao(comunicacao);
	}

}
