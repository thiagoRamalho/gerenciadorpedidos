package br.com.trama.gerenciadorpedidos.listeners;

import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import br.com.trama.gerenciadorpedidos.R;
import br.com.trama.gerenciadorpedidos.views.form.FormFragmentViewPagerTemplate;
import br.com.trama.gerenciadorpedidos.views.form.FormPageAdapter;

public class NextClickListener implements OnClickListener {

		private ViewPager viewPager;
		private Button btnNext;
		private Button btnPrevious;
		private FormPageAdapter pageAdapter;
		
		public NextClickListener(ViewPager viewPager, Button btnNext,
				Button btnPrevious, FormPageAdapter pageAdapter) {
			super();
			this.viewPager = viewPager;
			this.btnNext = btnNext;
			this.btnPrevious = btnPrevious;
			this.pageAdapter = pageAdapter;
			
			this.init();
		}

		private void init(){
			
			int maxPage = pageAdapter.getCount() -1;
			int currentItem = viewPager.getCurrentItem();

			btnPrevious.setEnabled(currentItem > 0);
			
			if(currentItem == maxPage){
				btnNext.setText(R.string.finish);
			}
		}
		
		@Override
		public void onClick(View v) {
			
			int currentItem = viewPager.getCurrentItem();
			
			FormFragmentViewPagerTemplate<?> item = pageAdapter.getItem(currentItem);
			
			if(item.next()){
				
				int maxPage = pageAdapter.getCount() -1;
				
				if(currentItem < maxPage){
					currentItem++;
				}
				
				if(currentItem == maxPage){
					btnNext.setText(R.string.finish);
				}
				
				btnPrevious.setEnabled(currentItem > 0);
				viewPager.setCurrentItem(currentItem);
			}
		}
	}