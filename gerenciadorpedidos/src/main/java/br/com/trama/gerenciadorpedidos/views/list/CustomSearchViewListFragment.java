package br.com.trama.gerenciadorpedidos.views.list;

import java.io.Serializable;

import roboguice.inject.InjectView;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import br.com.trama.gerenciadorpedidos.R;
import br.com.trama.gerenciadorpedidos.util.ConstantsUtil;
import br.com.trama.gerenciadorpedidos.util.RouterUtil;
import br.com.trama.gerenciadorpedidos.views.form.fornecedor.OperationType;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.widget.SearchView;
import com.actionbarsherlock.widget.SearchView.OnQueryTextListener;
import com.github.rtyley.android.sherlock.roboguice.fragment.RoboSherlockFragment;
import com.google.inject.Inject;

public abstract class CustomSearchViewListFragment<T> extends RoboSherlockFragment 
implements OnQueryTextListener, com.actionbarsherlock.view.MenuItem.OnActionExpandListener{
	
	private final String TAG = CustomSearchViewListFragment.class.getSimpleName();

	protected SearchView mSearchView;

	@Inject
	protected RouterUtil routerUtil;

	@InjectView(R.id.defaultListView) 
	protected ListView listView;

	protected T entity;

	protected OperationType operationType = OperationType.ALL;

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		
		registerForContextMenu(listView);
		
		listView.setOnItemClickListener(new OnContextClickListener());
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		setHasOptionsMenu(true);

		Bundle arguments = getArguments();

		configTitles(arguments);

		return inflater.inflate(R.layout.default_list_layout, container, false);
	}
	
	@Override
	public void onPause() {
		Log.d(TAG, "onPause");
		
		mSearchView.setOnQueryTextListener(null);
		mSearchView.setQuery("", false); 
		
		super.onPause();
	}

	private void configTitles(Bundle arguments) {
		
		if(arguments != null){
			
			if(arguments.containsKey(ConstantsUtil.TITLE_FRAGMENT)){
				String title = arguments.getString(ConstantsUtil.TITLE_FRAGMENT);
				getSherlockActivity().getSupportActionBar().setTitle(title);
			}

			if(arguments.containsKey(ConstantsUtil.SUBTITLE_FRAGMENT)){
				String subTitle = arguments.getString(ConstantsUtil.SUBTITLE_FRAGMENT);
				getSherlockActivity().getSupportActionBar().setSubtitle(subTitle);
			}
		}
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

		Log.d(TAG, "onCreateOptionsMenu");

		inflater.inflate(R.menu.action_bar_menu, menu);

		createMenu(menu);
	}

	protected void createMenu(Menu menu) {
		
		//Create the search view
        SearchView searchView = new SearchView(getActivity());
        searchView.setQueryHint("");
        searchView.setOnQueryTextListener(this);
		
		mSearchView = new SearchView(this.getActivity()); 
		
		mSearchView.setQuery("", false); 
		mSearchView.setQueryHint(getString(R.string.search_hint));
		mSearchView.setOnQueryTextListener(this);
		
		MenuItem search = menu.findItem(R.id.search_id_menu_action_bar);

		search.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM | MenuItem.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW);
		search.setActionView(mSearchView);

		search.setOnActionExpandListener(this);
		
		MenuItem add = menu.findItem(R.id.add_id_menu_action_bar);

		add.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
		
		MenuItem refresh = menu.findItem(R.id.refresh_id_menu_action_bar);
		refresh.setVisible(false);
	}

	@Override
	public boolean onQueryTextChange(String arg0) {

		Log.d(TAG, "onQueryTextChange ["+arg0+"]");

		return true;
	}

	@Override
	public boolean onQueryTextSubmit(String arg0) {

		Log.d(TAG, "onQueryTextSubmit ["+arg0+"]");

		return true;
	}
	
	public void setOperation(OperationType operationType) {
		this.operationType  = operationType;
	}


	@SuppressWarnings("hiding")
	protected <T extends Serializable> void runTo(Class<?> clazz, T entity, Long id) {
		routerUtil
		.withClass(clazz)
		.withContext(this.getActivity())
		.andExtra(ConstantsUtil.ENTITY_TYPE, entity)
		.andExtra(ConstantsUtil.ENTITY_ID, id)
		.route();
	}

	class OnContextClickListener implements OnItemClickListener{

		@SuppressWarnings("unchecked")
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			entity = (T) listView.getItemAtPosition(position);
			listView.showContextMenu();
		}
	}

	@Override
	public boolean onMenuItemActionExpand(MenuItem item) {
		
		Log.d(TAG, "onMenuItemActionExpand");

		return true;
	}

	@Override
	public boolean onMenuItemActionCollapse(MenuItem item) {
		
		Log.d(TAG, "onMenuItemActionExpand");
		
		mSearchView.setQuery("", false);
		
		return true;
	}
}
