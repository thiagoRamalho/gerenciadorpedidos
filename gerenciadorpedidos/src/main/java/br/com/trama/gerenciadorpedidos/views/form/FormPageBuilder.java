package br.com.trama.gerenciadorpedidos.views.form;

import br.com.trama.gerenciadorpedidos.model.entity.Fornecedor;
import br.com.trama.gerenciadorpedidos.model.entity.Produto;
import br.com.trama.gerenciadorpedidos.views.form.clientes.ClienteFormBuilderStrategy;
import br.com.trama.gerenciadorpedidos.views.form.fornecedor.FornecedorFormBuilderStrategy;
import br.com.trama.gerenciadorpedidos.views.form.produto.ProdutoFormBuilderStrategy;


public class FormPageBuilder {
	
	private Long id;
	private Class<?> entityType;
	private FormBuilderStrategy strategy;
	
	public FormPageBuilder withId(Long id){
		this.id = id;
		return this;
	}

	public FormPageBuilder withType(Class<?> entityType) {
		this.entityType = entityType;
		return this;
	}

	public FormBuilderStrategy create() {

		if(entityType == null){
			throw new IllegalArgumentException("entityType don't be null");
		}
		
		strategy = new ClienteFormBuilderStrategy(this.id);
		
		if(Produto.class.getSimpleName().equals(entityType.getSimpleName())){
			strategy = new ProdutoFormBuilderStrategy(this.id);
		}
		else if(Fornecedor.class.getSimpleName().equals(entityType.getSimpleName())){
			strategy = new FornecedorFormBuilderStrategy(this.id);
		}
		
		return strategy;
	}

	public void save() {
		this.strategy.save();
	}

}
