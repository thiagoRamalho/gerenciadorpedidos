package br.com.trama.gerenciadorpedidos.views.list;

import java.util.List;

public interface SearchStrategy<T>{
	public List<T> search();
	public List<T> searchBy(String text);
}
