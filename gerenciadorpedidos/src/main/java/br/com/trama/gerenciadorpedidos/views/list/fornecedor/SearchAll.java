package br.com.trama.gerenciadorpedidos.views.list.fornecedor;

import java.util.List;

import br.com.trama.gerenciadorpedidos.model.dao.FornecedorDAO;
import br.com.trama.gerenciadorpedidos.model.entity.Fornecedor;
import br.com.trama.gerenciadorpedidos.views.list.SearchStrategy;

class SearchAll implements SearchStrategy<Fornecedor>{

	private FornecedorDAO fornecedorDAO;
	
	public SearchAll(FornecedorDAO fornecedorDAO) {
		super();
		this.fornecedorDAO = fornecedorDAO;
	}

	@Override
	public List<Fornecedor> search() {
		return fornecedorDAO.findAll();
	}

	@Override
	public List<Fornecedor> searchBy(String text) {
		return fornecedorDAO.findByNome(text);
	}
}
