package br.com.trama.gerenciadorpedidos.views.form.fornecedor;

import java.util.List;

import roboguice.inject.InjectView;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import br.com.trama.gerenciadorpedidos.R;
import br.com.trama.gerenciadorpedidos.model.dao.FornecedorDAO;
import br.com.trama.gerenciadorpedidos.model.entity.Fornecedor;
import br.com.trama.gerenciadorpedidos.views.form.FormFragmentViewPagerTemplate;

import com.google.inject.Inject;

public class FornecedorFragment extends FormFragmentViewPagerTemplate<Fornecedor>{

	private final String TAG = FornecedorFragment.class.getSimpleName();

	@InjectView(R.id.nome_fornec_edit)
	private EditText nome;

	@InjectView(R.id.email_fornec_edit)
	private EditText email;

	@InjectView(R.id.linha_fornec_edit)
	private EditText linhaProdutos;

	@InjectView(R.id.ativo_fornec_edit)
	private CheckBox ativo;

	@Inject
	private FornecedorDAO fornecedorDAO;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		return inflater.inflate(R.layout.fornecedor_form_layout, container, false);
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {

		super.onViewCreated(view, savedInstanceState);

		Log.d(TAG, "onViewCreated");

		populateForm(savedInstanceState);
	}

	private void populateForm(Bundle savedInstanceState) {

		Log.d(TAG, "populateForm");

		getEntity(savedInstanceState);

		nome.setText(super.entity.getNome());

		email.setText(super.entity.getEmail());

		linhaProdutos.setText(super.entity.getLinhaProdutos());

		ativo.setChecked(super.entity.isAtivo());
	}

	protected void populateEntity() {

		super.entity.setNome(nome.getText().toString());

		super.entity.setEmail(email.getText().toString());

		super.entity.setLinhaProdutos(linhaProdutos.getText().toString());

		super.entity.setAtivo(ativo.isChecked());
	}

	protected boolean validate() {

		boolean isOk = true;

		if(!validateNome()){
			isOk = false;
		}

		if(!validateEmail()){
			isOk = false;
		}

		if(!validateLinha()){
			isOk = false;
		}

		return isOk;
	}

	private boolean validateNome() {

		boolean isOk = true;

		if(super.componentUtil.isEmpty(nome)){
			nome.setError(invalidValue);
			isOk = false;
		}

		if(isDuplicateValue(nome)){
			nome.setError(duplicateValue);
			isOk = false;
		}

		return isOk;
	}



	private boolean validateEmail() {		

		boolean isOk = true;

		if(!super.componentUtil.validateEmail(email, false)){
			email.setError(super.invalidValue);
			isOk = false;
		}

		return isOk;
	}
	
	private boolean validateLinha() {

		boolean isOk = true;

		if(super.componentUtil.isEmpty(linhaProdutos)){
			linhaProdutos.setError(invalidValue);
			isOk = false;
		}

		return isOk;
	}



	private boolean isDuplicateValue(TextView searchValue) {

		boolean isOk = false;

		String extractValue = super.componentUtil.extractValue(searchValue);

		List<Fornecedor> result = this.fornecedorDAO.findByNome(extractValue);

		for (Fornecedor entidade : result) {

			if(!entidade.getId().equals(super.entity.getId())){
				isOk = true;
				break;
			}
		}

		return isOk;
	}
}
