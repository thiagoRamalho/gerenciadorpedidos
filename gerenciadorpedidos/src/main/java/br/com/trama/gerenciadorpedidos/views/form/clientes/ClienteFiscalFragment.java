package br.com.trama.gerenciadorpedidos.views.form.clientes;

import roboguice.inject.InjectResource;
import roboguice.inject.InjectView;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import br.com.trama.gerenciadorpedidos.R;
import br.com.trama.gerenciadorpedidos.model.QueryUtil;
import br.com.trama.gerenciadorpedidos.util.ConstantsUtil;
import br.com.trama.gerenciadorpedidos.validator.CNPJValidator;
import br.com.trama.gerenciadorpedidos.validator.IEValidator;
import br.com.trama.maskinput.MaskWatcher;

import com.google.inject.Inject;

public class ClienteFiscalFragment extends ClienteFormTemplate{

	private final String TAG = ClienteFiscalFragment.class.getSimpleName();

	@InjectResource(R.string.msg_invalid_verifier_digit)
	private String invalidVerifierDigit;
	
	@InjectView(R.id.nome_fantasia_edit)
	private EditText nomeFantasia;

	@InjectView(R.id.razao_social_edit)
	private EditText razaoSocial;

	@InjectView(R.id.cnpj_edit)
	private EditText cnpj;

	@InjectView(R.id.insc_estadual_edit)
	private EditText ie;
	
	@InjectView(R.id.insc_municipal_edit)
	private EditText im;
	
	@Inject
	private CNPJValidator cnpjValidator;

	@Inject
	private IEValidator ieValidator;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		return inflater.inflate(R.layout.cliente_fiscal_form_layout, container, false);
	}
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		
		super.onViewCreated(view, savedInstanceState);
		
		Log.d(TAG, "onViewCreated");
		
		configComponents();
		
		populateForm(savedInstanceState);
	}

	private void populateForm(Bundle savedInstanceState) {
		
		Log.d(TAG, "populateForm");
		
		getEntity(savedInstanceState);
		
		nomeFantasia.setText(super.entity.getNomeFantasia());
		
		razaoSocial.setText(super.entity.getRazaoSocial());
		
		cnpj.setText(super.entity.getCnpj());
		
		ie.setText(super.entity.getInscricaoEstadual());
		
		im.setText(super.entity.getInscricaoMunicipal());
	}


	private void configComponents() {
		
		Log.d(TAG, "configComponents");
		
		cnpj.addTextChangedListener(new MaskWatcher(ConstantsUtil.MASK_CNPJ, ConstantsUtil.DENIEL_NUMBERS_REGEX));
		cnpj.setRawInputType(InputType.TYPE_CLASS_NUMBER);

		ie.addTextChangedListener  (new MaskWatcher(ConstantsUtil.MASK_IE, ConstantsUtil.DENIEL_NUMBERS_REGEX));
		ie.setRawInputType(InputType.TYPE_CLASS_NUMBER);
	}


	protected void populateEntity() {
		
		super.entity.setNomeFantasia(super.componentUtil.extractValue(nomeFantasia));
		
		super.entity.setRazaoSocial(super.componentUtil.extractValue(razaoSocial));

		super.entity.setCnpj(super.componentUtil.extractValue(cnpj, ConstantsUtil.DENIEL_NUMBERS_REGEX));

		super.entity.setInscricaoEstadual(super.componentUtil.extractValue(ie, ConstantsUtil.DENIEL_NUMBERS_REGEX));
		
		super.entity.setInscricaoMunicipal(super.componentUtil.extractValue(im, ConstantsUtil.DENIEL_NUMBERS_REGEX));

	}

	protected boolean validate() {
		
		boolean isOk = true;
		
		if(!validateNomeFantasia()){
			isOk = false;
		}

		if(!validateRazaoSocial()){
			isOk = false;
		}

		if(!validateCNPJ()){
			isOk = false;
		}
		
		if(!validateIE()){
			isOk = false;
		}
		
		if(!validateIM()){
			isOk = false;
		}
		
		return isOk;
	}


	private boolean validateRazaoSocial() {
		
		boolean isOk = true;
		
		if(super.componentUtil.isEmpty(razaoSocial)){
			razaoSocial.setError(invalidValue);
			isOk = false;
		}

		if(isDuplicateValue(razaoSocial, QueryUtil.BY_RAZAO_SOCIAL)){
			razaoSocial.setError(duplicateValue);
			isOk = false;
		}
		
		return isOk;
	}
	
	private boolean validateNomeFantasia() {
		
		boolean isOk = true;
		
		if(super.componentUtil.isEmpty(nomeFantasia)){
			nomeFantasia.setError(invalidValue);
			isOk = false;
		}

		if(isDuplicateValue(nomeFantasia, QueryUtil.BY_NOME_FANTASIA)){
			nomeFantasia.setError(duplicateValue);
			isOk = false;
		}
		
		return isOk;
	}


	private boolean validateCNPJ() {
		
		boolean isOk = true;
		
		if(super.componentUtil.isEmpty(cnpj)){
			cnpj.setError(invalidValue);
			isOk = false;
		}
		else if(!this.cnpjValidator.isValid(cnpj)){
			cnpj.setError(invalidVerifierDigit);
			isOk = false;
		}
		else if(isDuplicateValue(cnpj, QueryUtil.BY_CNPJ, ConstantsUtil.DENIEL_NUMBERS_REGEX)){
			cnpj.setError(duplicateValue);
			isOk = false;
		}
		
		return isOk;
	}
	
	private boolean validateIE() {
		
		boolean isOk = true;
		
		if(super.componentUtil.isEmpty(ie)){
			ie.setError(invalidValue);
			isOk = false;
		}
		else if(!this.ieValidator.isValid(ie)){
			ie.setError(invalidVerifierDigit);
			isOk = false;
		}
		else if(isDuplicateValue(ie, QueryUtil.BY_IE, ConstantsUtil.DENIEL_NUMBERS_REGEX)){
			ie.setError(duplicateValue);
			isOk = false;
		}

		return isOk;
	}

	private boolean validateIM() {

		boolean isOk = true;

		if(!super.componentUtil.isEmpty(im)){

			if(isDuplicateValue(ie, QueryUtil.BY_IM)){
				ie.setError(duplicateValue);
				isOk = false;
			}
		}

		return isOk;
	}
}
