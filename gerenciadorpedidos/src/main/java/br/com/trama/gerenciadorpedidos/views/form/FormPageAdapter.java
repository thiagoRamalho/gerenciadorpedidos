package br.com.trama.gerenciadorpedidos.views.form;

import java.util.ArrayList;
import java.util.List;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class FormPageAdapter extends  FragmentPagerAdapter{

	private List<? extends FormFragmentViewPagerTemplate<?>> arrayList;

	public FormPageAdapter(FragmentManager fm) {
		super(fm);
		arrayList = new ArrayList<FormFragmentViewPagerTemplate<?>>();
	}

	@Override
	public FormFragmentViewPagerTemplate<?> getItem(int position) {
		return arrayList.get(position);
	}

	@Override
	public int getCount() {
		return this.arrayList.size();
	}

	public void set(List<? extends FormFragmentViewPagerTemplate<?>> formFragment) {
		this.arrayList = formFragment;
	}
}
