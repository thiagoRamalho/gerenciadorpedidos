package br.com.trama.gerenciadorpedidos.model.dao;

import java.util.ArrayList;
import java.util.List;

import br.com.trama.gerenciadorpedidos.model.QueryUtil;
import br.com.trama.gerenciadorpedidos.model.entity.Fornecedor;

public class FornecedorDAO {

	public List<Fornecedor> findByAtivo(boolean isAtivo){
		
		String[] filter = {isAtivo ? "1" : "0"};
		
		return Fornecedor.find(Fornecedor.class, QueryUtil.BY_ATIVO, filter);
	}

	public List<Fornecedor> findAll() {
		return Fornecedor.listAll(Fornecedor.class);
	}

	public List<Fornecedor> findByNome(String text) {
		
		List<Fornecedor> fornecedores = new ArrayList<Fornecedor>();
		
		if(isTextOk(text)){
			fornecedores = Fornecedor.find(Fornecedor.class, QueryUtil.BY_NOME, QueryUtil.like(text));
		}
		
		return fornecedores;
		
	}

	public long count(boolean isAtivo) {
		String[] filter = {isAtivo ? "1" : "0"};
		return Fornecedor.count(Fornecedor.class, QueryUtil.BY_ATIVO, filter);
	}

	public List<Fornecedor> findByNomeAndAtivo(String text, boolean isAtivo) {
		
		String[] filter = {QueryUtil.like(text), isAtivo ? "1" : "0"};
		
		List<Fornecedor> fornecedores = new ArrayList<Fornecedor>();
		
		if(isTextOk(text)){
			fornecedores = Fornecedor.find(Fornecedor.class, QueryUtil.BY_NOME_AND_ATIVO, filter);
		}
		
		return fornecedores;
	}

	private boolean isTextOk(String text) {
		return text != null && text.trim().length() > 0;
	}
}
