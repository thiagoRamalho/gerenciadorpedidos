package br.com.trama.gerenciadorpedidos.views.form;

import java.io.Serializable;

import roboguice.fragment.RoboFragment;
import roboguice.inject.InjectResource;
import android.os.Bundle;
import br.com.trama.gerenciadorpedidos.R;
import br.com.trama.gerenciadorpedidos.util.ComponentUtil;
import br.com.trama.gerenciadorpedidos.util.ConstantsUtil;

import com.google.inject.Inject;

public abstract class FormFragmentViewPagerTemplate<T extends Serializable> extends RoboFragment{

	@Inject
	protected ComponentUtil componentUtil;
	
	@InjectResource(R.string.msg_invalid_value)
	protected String invalidValue;

	@InjectResource(R.string.msg_duplicate_value)
	protected String duplicateValue;
	
	protected T entity;
	
	public void setEntity(T entity) {
		this.entity = entity;
	}
	
	public boolean next() {
		
		boolean isOk = true;

		isOk = validate();
		
		if(isOk){
			populateEntity();
		}
		
		return isOk;
	}

	
	protected abstract boolean validate();

	protected abstract void populateEntity();
	
	

	@Override
	public void onSaveInstanceState(Bundle outState) {
		
		super.onSaveInstanceState(outState);
		
		outState.putSerializable(ConstantsUtil.CACHE_ENTITY, entity);
	}
	
	@SuppressWarnings("unchecked")
	protected void getEntity(Bundle savedInstanceState) {

		if(savedInstanceState != null){
			
			Serializable serializable = savedInstanceState.getSerializable(ConstantsUtil.CACHE_ENTITY);

			if(serializable != null){
				setEntity((T)serializable);
			}
		}
	}
}
