package br.com.trama.gerenciadorpedidos.views.list.pedidos;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Toast;
import br.com.trama.gerenciadorpedidos.R;
import br.com.trama.gerenciadorpedidos.mail.SendMailIntentService;
import br.com.trama.gerenciadorpedidos.model.QueryUtil;
import br.com.trama.gerenciadorpedidos.model.entity.Carrinho;
import br.com.trama.gerenciadorpedidos.model.entity.Pedido;
import br.com.trama.gerenciadorpedidos.util.ConstantsUtil;
import br.com.trama.gerenciadorpedidos.views.form.pedido.FormPedidoFragmentActivity;
import br.com.trama.gerenciadorpedidos.views.list.CustomSearchViewListFragment;
import br.com.trama.gerenciadorpedidos.views.list.fornecedor.FornecedorManager;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.google.inject.Inject;


public class PedidoListFragment extends CustomSearchViewListFragment<Pedido>{

	private final String TAG = PedidoListFragment.class.getSimpleName();

	@Inject
	private FornecedorManager fornecedorManager;

	private SendMailBroadcast sendMailBroadcast;

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		listView.setOnItemClickListener(new OnPedidoClickListener());
	}

	@Override
	public void onResume() {
		
		updateList();
		
		sendMailBroadcast = new SendMailBroadcast();
		
		IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction(ConstantsUtil.BROADCAST_SEND_MAIL);
		
		getActivity().registerReceiver(sendMailBroadcast, intentFilter);

		super.onResume();
	}

	private void updateList() {
		List<Pedido> findAll = Pedido.findWithQuery(Pedido.class, QueryUtil.FROM_PEDIDO_DESC);
		setData(findAll);
	}

	@Override
	public void onPause() {
		getActivity().unregisterReceiver(sendMailBroadcast);
		super.onPause();
	}

	protected void setData(List<Pedido> findAll) {
		listView.setAdapter(new PedidosAdapter(findAll, getActivity()));
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);

		MenuItem search = menu.findItem(R.id.search_id_menu_action_bar);
		search.setVisible(false);
		
		MenuItem refresh = menu.findItem(R.id.refresh_id_menu_action_bar);
		refresh.setVisible(true);
		refresh.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		Log.d(TAG, "onOptionsItemSelected ["+item.getItemId()+"]");

		if(item.getItemId() == R.id.add_id_menu_action_bar){

			Carrinho.clean();
			runTo(FormPedidoFragmentActivity.class, Pedido.class, null);
			
			return true;
		}
		else if(item.getItemId() == R.id.refresh_id_menu_action_bar){
			
			item.setActionView(R.layout.refresh_action_view);
			
			Toast.makeText(getActivity(), R.string.msg_init_send_mail, Toast.LENGTH_LONG).show();
			
			routerUtil
			.withContext(getActivity())
			.withService(SendMailIntentService.class)
			.startService();
			
			return true;
		}

		return super.onOptionsItemSelected(item);
	}

	@SuppressWarnings("hiding")
	private <Pedido extends Serializable> void runToExtra() {

		routerUtil
		.withClass(FormPedidoFragmentActivity.class)
		.withContext(this.getActivity())
		.andExtra(ConstantsUtil.ACTION_EDIT, true)
		.route();
	}

	class OnPedidoClickListener implements OnItemClickListener{

		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {

			Pedido p = (Pedido) parent.getItemAtPosition(position);

			p.sincItens();

			Carrinho.clean();
			Carrinho.setPedido(p);

			runToExtra();
		}
	}

	private class SendMailBroadcast extends BroadcastReceiver{

		@Override
		public void onReceive(Context context, Intent intent) {
			
			Bundle extras = intent.getExtras();
			
			if(extras != null && extras.containsKey(ConstantsUtil.TOTAL_SEND_MAIL)){
				
				int total = extras.getInt(ConstantsUtil.TOTAL_SEND_MAIL);

				String msg = context.getString(R.string.msg_total_send_mail, total);
				
				Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
				
				if(total > BigDecimal.ZERO.intValue()){
					updateList();
				}
				
				getActivity().supportInvalidateOptionsMenu();
			}
		}
	}
}
