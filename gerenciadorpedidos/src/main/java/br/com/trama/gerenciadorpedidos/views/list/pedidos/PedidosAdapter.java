package br.com.trama.gerenciadorpedidos.views.list.pedidos;

import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import br.com.trama.gerenciadorpedidos.R;
import br.com.trama.gerenciadorpedidos.model.entity.Pedido;
import br.com.trama.gerenciadorpedidos.util.ComponentUtil;

@SuppressLint("ViewTag")
public class PedidosAdapter extends BaseAdapter{

	private List<Pedido> pedidos;
	private LayoutInflater inflater;
	private ComponentUtil componentUtil;
	
	public PedidosAdapter(List<Pedido> pedidos, Context context) {
		super();
		this.pedidos = pedidos;
		this.inflater = LayoutInflater.from(context);
		componentUtil = new ComponentUtil();
	}

	@Override
	public int getCount() {
		return this.pedidos.size();
	}

	@Override
	public Pedido getItem(int position) {
		return this.pedidos.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		
		TextView pedidoId, nomeFantasia, dataCriacao, total;
		View sincStatus;
		
		if (convertView == null) {
			
			convertView = inflater.inflate(R.layout.row_pedido_layout, parent, false);
			
			total       = (TextView)  convertView.findViewById(R.id.pedido_faturado_total_id);
			pedidoId    = (TextView)  convertView.findViewById(R.id.pedido_id);
			nomeFantasia = (TextView)  convertView.findViewById(R.id.pedido_nome_fantasia_id);
			dataCriacao = (TextView)  convertView.findViewById(R.id.pedido_data_criacao_id);
			sincStatus  = (View) convertView.findViewById(R.id.status_shape);

			convertView.setTag(R.id.pedido_faturado_total_id, total);
			convertView.setTag(R.id.pedido_id, pedidoId);
			convertView.setTag(R.id.pedido_nome_fantasia_id, nomeFantasia);
			convertView.setTag(R.id.pedido_data_criacao_id, dataCriacao);
			convertView.setTag(R.id.status_shape, sincStatus);
		
		} else {
			total       = (TextView)  convertView.getTag(R.id.pedido_faturado_total_id);
			pedidoId    = (TextView)  convertView.getTag(R.id.pedido_id);
			nomeFantasia = (TextView)  convertView.getTag(R.id.pedido_nome_fantasia_id);
			dataCriacao = (TextView)  convertView.getTag(R.id.pedido_data_criacao_id);
			sincStatus  = (View)      convertView.getTag(R.id.status_shape);
		}
		
		Pedido entidade = this.getItem(position);
		
		pedidoId.setText(""+entidade.getId().longValue());
		nomeFantasia.setText(entidade.getCliente().getNomeFantasia());
		dataCriacao.setText(componentUtil.calendarToString(entidade.getDate()));
		total.setText(componentUtil.doubleToCurrency(entidade.getTotal()));
		
		int rId = entidade.isSincronizado() ? R.color.sync_color : R.color.not_sync_color;
		
		sincStatus.setBackgroundResource(rId);
		
		return convertView;
	}
}
