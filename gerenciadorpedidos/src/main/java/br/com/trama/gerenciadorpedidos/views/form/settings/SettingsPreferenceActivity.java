package br.com.trama.gerenciadorpedidos.views.form.settings;

import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.Preference;
import android.support.v4.app.NavUtils;
import android.text.TextUtils;
import br.com.trama.gerenciadorpedidos.R;

import com.actionbarsherlock.view.MenuItem;
import com.github.rtyley.android.sherlock.roboguice.activity.RoboSherlockPreferenceActivity;

public class SettingsPreferenceActivity extends RoboSherlockPreferenceActivity implements OnSharedPreferenceChangeListener{ 

	private TypedArray obtainTypedArray;
	private String keyPass;

	@SuppressWarnings("deprecation")
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.preferences);
		
		obtainTypedArray = getResources().obtainTypedArray(R.array.replace_summary_settings);  
		obtainTypedArray.recycle();
		
		keyPass = getString(R.string.pref_smtp_pass_key);
		
		setSummary();

		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setHomeButtonEnabled(true);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    switch (item.getItemId()) {
	    // Respond to the action bar's Up/Home button
	    case android.R.id.home:
	        NavUtils.navigateUpFromSameTask(this);
	        return true;
	    }
	    
	    return super.onOptionsItemSelected(item);
	}

	private void setSummary() {
		
		int lenght = obtainTypedArray.length();
		
		for (int index = 0; index < lenght; index++) {
			
			int resourceId = obtainTypedArray.getResourceId(index, -1);
			
			@SuppressWarnings("deprecation")
			EditTextPreference preference = (EditTextPreference) findPreference(getString(resourceId));
			
			if(!TextUtils.isEmpty(preference.getText())){
		        preference.setSummary(preference.getText());
			}
		}
	}

	@Override
	public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {

		@SuppressWarnings("deprecation")
		Preference connectionPref = findPreference(key);
		
		if(connectionPref instanceof EditTextPreference && !key.equals(keyPass)){
			connectionPref.setSummary(sharedPreferences.getString(key, ""));
		}
	}


	@Override
	protected void onResume() {
		super.onResume();

		getSharedPreference()
		.registerOnSharedPreferenceChangeListener(this);
	}

	@SuppressWarnings("deprecation")
	private SharedPreferences getSharedPreference() {
		return getPreferenceScreen().getSharedPreferences();
	}

	@Override
	protected void onPause() {
		super.onPause();
		getSharedPreference()
		.unregisterOnSharedPreferenceChangeListener(this);
	}
}
