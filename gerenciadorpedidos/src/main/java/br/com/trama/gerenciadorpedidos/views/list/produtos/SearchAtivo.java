package br.com.trama.gerenciadorpedidos.views.list.produtos;

import java.util.List;

import br.com.trama.gerenciadorpedidos.model.dao.ProdutoDAO;
import br.com.trama.gerenciadorpedidos.model.entity.Produto;
import br.com.trama.gerenciadorpedidos.views.list.SearchStrategy;

public class SearchAtivo implements SearchStrategy<Produto>{

	private ProdutoDAO produtoDAO;
	
	public SearchAtivo(ProdutoDAO produtoDAO) {
		super();
		this.produtoDAO = produtoDAO;
	}

	@Override
	public List<Produto> search() {
		return this.produtoDAO.findByAtivo(true);
	}

	@Override
	public List<Produto> searchBy(String text) {
		return this.produtoDAO.findByNomeAndAtivo(text, true);
	}
}
