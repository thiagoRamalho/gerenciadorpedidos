package br.com.trama.gerenciadorpedidos;

import java.util.List;

import roboguice.inject.ContentView;
import roboguice.inject.InjectResource;
import roboguice.inject.InjectView;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import br.com.trama.gerenciadorpedidos.drawer.NavDrawerItem;
import br.com.trama.gerenciadorpedidos.drawer.NavDrawerListAdapter;
import br.com.trama.gerenciadorpedidos.drawer.NavItemGenerator;
import br.com.trama.gerenciadorpedidos.util.ConstantsUtil;
import br.com.trama.gerenciadorpedidos.util.RouterUtil;
import br.com.trama.gerenciadorpedidos.views.form.settings.SettingsPreferenceActivity;
import br.com.trama.gerenciadorpedidos.views.list.clientes.ClientesListFragment;
import br.com.trama.gerenciadorpedidos.views.list.fornecedor.FornecedorListFragment;
import br.com.trama.gerenciadorpedidos.views.list.log.LogErrorListFragment;
import br.com.trama.gerenciadorpedidos.views.list.pedidos.PedidoListFragment;
import br.com.trama.gerenciadorpedidos.views.list.produtos.ProdutosListFragment;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.view.MenuItem;
import com.github.rtyley.android.sherlock.roboguice.activity.RoboSherlockFragmentActivity;
import com.google.inject.Inject;

@ContentView(R.layout.activity_main)
public class MainActivity extends RoboSherlockFragmentActivity{

	private final String TAG = MainActivity.class.getSimpleName();

	@InjectView(R.id.drawer_layout) 
	private DrawerLayout drawerLayout;

	@InjectView(R.id.list_slidermenu) 
	private ListView drawerList;

	@InjectResource(R.array.nav_drawer_items)
	private String[] navDrawerItens;

	@Inject
	private NavItemGenerator navItemGenerator;

	@Inject
	private RouterUtil routerUtil;

	private ActionBarDrawerToggle mDrawerToggle;

	private CharSequence mTitle;

	private CharSequence mDrawerTitle;


	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		mTitle = mDrawerTitle = getTitle();

		drawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);

		TypedArray obtainTypedArray = getResources().obtainTypedArray(R.array.nav_drawer_icons);  

		List<NavDrawerItem> navItens = navItemGenerator.execute(navDrawerItens, obtainTypedArray);
		obtainTypedArray.recycle();

		drawerList.setAdapter(new NavDrawerListAdapter(this, navItens));

		// Set the list's click listener
		drawerList.setOnItemClickListener(new DrawerItemClickListener());

		mDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout,
				R.drawable.ic_drawer, //nav menu toggle icon
				R.string.app_name, // nav drawer open - description for accessibility
				R.string.app_name // nav drawer close - description for accessibility
				){
			public void onDrawerClosed(View view) {
				getSupportActionBar().setTitle(mTitle);
				supportInvalidateOptionsMenu();
			}

			public void onDrawerOpened(View drawerView) {
				ActionBar supportActionBar = getSupportActionBar();
				supportActionBar.setTitle(mDrawerTitle);
				supportInvalidateOptionsMenu();
			}
		};
		// Set the drawer toggle as the DrawerListener
		drawerLayout.setDrawerListener(mDrawerToggle);


		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setHomeButtonEnabled(true);

		if(savedInstanceState == null){
			displayView(ConstantsUtil.INDEX_NAV_PEDIDO);
		}
	}


	@Override
	public void setTitle(CharSequence title) {
		mTitle = title;
		getSupportActionBar().setTitle(title);
	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		// Sync the toggle state after onRestoreInstanceState has occurred.
		mDrawerToggle.syncState();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		mDrawerToggle.onConfigurationChanged(newConfig);
	}

	private void displayView(int position){

		Fragment target = null;

		Log.d(TAG, "selected index position ["+position+"]");

		switch (position) {
		case ConstantsUtil.INDEX_NAV_PEDIDO:
			target = new PedidoListFragment();
			break;
		case ConstantsUtil.INDEX_NAV_CLIENTE:
			target = new ClientesListFragment();
			break;
		case ConstantsUtil.INDEX_NAV_FORNECE:
			target = new FornecedorListFragment();
			break;
		case ConstantsUtil.INDEX_NAV_PRODUTO:
			target = new ProdutosListFragment();
			break;
		case ConstantsUtil.INDEX_NAV_ROTEIRO:
			target = null;
			break;
		case ConstantsUtil.INDEX_NAV_LOG:
			target = new LogErrorListFragment();
			break;
		case ConstantsUtil.INDEX_NAV_CONFIG:
			this.routerUtil
			.withClass(SettingsPreferenceActivity.class)
			.withContext(this)
			.route();

			drawerList.setItemChecked(position, true);
			drawerList.setSelection(position);
			setTitle(navDrawerItens[position]);
			drawerLayout.closeDrawer(drawerList);

			return;

		default:
			break;
		}

		drawerList.setItemChecked(position, true);
		drawerList.setSelection(position);
		setTitle(navDrawerItens[position]);
		drawerLayout.closeDrawer(drawerList);

		this.routerUtil.routeFragment(this, R.id.frame_container, target);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		if(item.getItemId()==android.R.id.home)
		{
			if(drawerLayout.isDrawerOpen(drawerList))
			{
				drawerLayout.closeDrawer(drawerList);
			}
			else {
				drawerLayout.openDrawer(drawerList);
			}
		}
		return super.onOptionsItemSelected(item);
	}

	class DrawerItemClickListener implements ListView.OnItemClickListener {

		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			displayView(position);
		}
	}
}

