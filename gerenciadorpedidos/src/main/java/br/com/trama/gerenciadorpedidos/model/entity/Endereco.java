package br.com.trama.gerenciadorpedidos.model.entity;

import java.io.Serializable;

import com.orm.SugarRecord;

public class Endereco extends SugarRecord<Endereco> implements Serializable{

	private static final long serialVersionUID = 1L;

	String logradouro;
	
	String numero;
	
	String complemento;
	
	String bairro;
	
	String cep;
	
	String cidade;
	
	String Uf;

	public String getLogradouro() {
		return logradouro;
	}

	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getComplemento() {
		return complemento;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	public String getUf() {
		return Uf;
	}

	public void setUf(String uf) {
		Uf = uf;
	}
}
