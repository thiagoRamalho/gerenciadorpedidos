package br.com.trama.gerenciadorpedidos.model.entity;

import java.io.Serializable;

import com.orm.SugarRecord;

public class Cliente  extends SugarRecord<Cliente> implements Serializable{

	private static final long serialVersionUID = 1L;

	private String nomeFantasia;
	
	private String razaoSocial;

	private String cnpj;

	private String inscricaoMunicipal;

	private String inscricaoEstadual;

	private Endereco endereco;

	private Comunicacao comunicacao;

	public void setNomeFantasia(String nomeFantasia) {
		this.nomeFantasia = nomeFantasia;
	}
	
	public String getNomeFantasia() {
		return nomeFantasia;
	}
	
	public String getRazaoSocial() {
		return razaoSocial;
	}

	public void setRazaoSocial(String razaoSocial) {
		this.razaoSocial = razaoSocial;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public String getInscricaoMunicipal() {
		return inscricaoMunicipal;
	}

	public void setInscricaoMunicipal(String inscricaoMunicipal) {
		this.inscricaoMunicipal = inscricaoMunicipal;
	}

	public String getInscricaoEstadual() {
		return inscricaoEstadual;
	}

	public void setInscricaoEstadual(String inscricaoEstadual) {
		this.inscricaoEstadual = inscricaoEstadual;
	}

	public Endereco getEndereco() {
		
		if(endereco == null){
			endereco = new Endereco();
		}
		
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	public Comunicacao getComunicacao() {
		
		if(comunicacao == null){
			comunicacao = new Comunicacao();
		}
		
		return comunicacao;
	}

	public void setComunicacao(Comunicacao comunicacao) {
		this.comunicacao = comunicacao;
	}

	@Override
	public String toString() {
		return this.nomeFantasia;
	}
}
