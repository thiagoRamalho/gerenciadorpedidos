package br.com.trama.gerenciadorpedidos.mail;

import java.io.File;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;

public interface SendMailBuilder {

	public abstract SendMailBuilder auth(String user, String password);

	public abstract SendMailBuilder from(String from);

	public abstract SendMailBuilder to(String to);

	public abstract SendMailBuilder cc(String cc);

	public abstract SendMailBuilder subject(String subject);

	public abstract boolean send() throws AddressException, MessagingException;

	public abstract SendMailBuilder withMessageBody(String messageBody);
	
	public abstract SendMailBuilder withAttach(File file);
 

}