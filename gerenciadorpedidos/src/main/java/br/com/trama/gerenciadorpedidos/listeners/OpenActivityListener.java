package br.com.trama.gerenciadorpedidos.listeners;

import android.view.View;
import android.view.View.OnClickListener;
import br.com.trama.gerenciadorpedidos.util.RouterUtil;

public class OpenActivityListener implements OnClickListener{

	private Class<?> clazz;
	private RouterUtil routerUtil;

	public OpenActivityListener(Class<?> clazz) {
		super();
		this.clazz = clazz;
		routerUtil = new RouterUtil();
	}

	public void onClick(View v) {
		
		routerUtil
		.withClass(clazz)
		.withContext(v.getContext())
		.route();
	}

}
