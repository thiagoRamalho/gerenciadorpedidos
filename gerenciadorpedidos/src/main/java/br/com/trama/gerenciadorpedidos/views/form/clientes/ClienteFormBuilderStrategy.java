package br.com.trama.gerenciadorpedidos.views.form.clientes;

import java.util.ArrayList;
import java.util.List;

import br.com.trama.gerenciadorpedidos.model.entity.Cliente;
import br.com.trama.gerenciadorpedidos.views.form.FormBuilderStrategy;
import br.com.trama.gerenciadorpedidos.views.form.FormFragmentViewPagerTemplate;

public class ClienteFormBuilderStrategy implements FormBuilderStrategy {

	private Long id;
	private Cliente cliente;

	public ClienteFormBuilderStrategy(Long id) {
		super();
		this.id = id;
	}

	@Override
	public List<FormFragmentViewPagerTemplate<Cliente>> getFormFragments() {

		cliente = new Cliente();

		if(id != null && id.intValue() > 0){
			cliente = Cliente.findById(Cliente.class, id);
		}

		List<FormFragmentViewPagerTemplate<Cliente>> list = new ArrayList<FormFragmentViewPagerTemplate<Cliente>>();

		ClienteFiscalFragment clienteFiscalFragment = new ClienteFiscalFragment();
		clienteFiscalFragment.setEntity(cliente);

		ClienteLocalizacaoFragment clienteLocalizacaoFragment = new ClienteLocalizacaoFragment();
		clienteLocalizacaoFragment.setEntity(cliente);

		ClienteComunicacaoFragment clienteComunicacaoFragment = new ClienteComunicacaoFragment();
		clienteComunicacaoFragment.setEntity(cliente);

		list.add(clienteFiscalFragment);
		list.add(clienteLocalizacaoFragment);
		list.add(clienteComunicacaoFragment);

		return list;
	}

	@Override
	public void save() {
		cliente.getEndereco().save();
		cliente.getComunicacao().save();
		cliente.save();
	}
}
