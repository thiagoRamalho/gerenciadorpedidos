package br.com.trama.gerenciadorpedidos.views.form;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

public class ViewPagerManager extends ViewPager {
	
	private boolean isTouchScroll = true;
	
	public ViewPagerManager(Context context, AttributeSet attrs) {
		super(context, attrs);
	}
	
	@Override
	public boolean onInterceptTouchEvent(MotionEvent ev) {
		if (isTouchScroll) {
			return super.onInterceptTouchEvent(ev);
		}
		
		return false;
	}
	
	public void setTouchScroll(boolean isTouchScroll) {
		this.isTouchScroll = isTouchScroll;
	}
}
