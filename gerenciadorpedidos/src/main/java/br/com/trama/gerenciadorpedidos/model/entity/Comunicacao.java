package br.com.trama.gerenciadorpedidos.model.entity;

import java.io.Serializable;

import com.orm.SugarRecord;

public class Comunicacao extends SugarRecord<Comunicacao> implements Serializable{

	private static final long serialVersionUID = 1L;

	String contato;
	
	long telefone;
	
	long celular;
	
	String nextel;

	String email;
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public long getTelefone() {
		return telefone;
	}

	public void setTelefone(long telefone) {
		this.telefone = telefone;
	}

	public long getCelular() {
		return celular;
	}

	public void setCelular(long celular) {
		this.celular = celular;
	}

	public String getNextel() {
		return nextel;
	}

	public void setNextel(String nextel) {
		this.nextel = nextel;
	}

	public String getContato() {
		return contato;
	}

	public void setContato(String contato) {
		this.contato = contato;
	}
	
	
}
