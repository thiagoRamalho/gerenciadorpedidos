package br.com.trama.gerenciadorpedidos.views.form;

import java.util.List;

public interface FormBuilderStrategy {

	List<? extends FormFragmentViewPagerTemplate<?>> getFormFragments();

	void save();
	
}
