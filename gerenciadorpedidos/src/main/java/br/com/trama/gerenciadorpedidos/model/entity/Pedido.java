package br.com.trama.gerenciadorpedidos.model.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import com.orm.SugarRecord;
import com.orm.dsl.Ignore;

public class Pedido extends SugarRecord<Pedido> implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private Cliente cliente;
	private Calendar date;
	private boolean isSincronizado;
	private String observacoes;
	private double total = 0;
	private Fornecedor fornecedor;
	private boolean isAVista;
	
	@Ignore
	private List<ItemPedido> itens = new ArrayList<ItemPedido>();

	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}
	
	public void setAVista(boolean isAVista) {
		this.isAVista = isAVista;
	}
	
	public boolean isAVista() {
		return isAVista;
	}
	
	public Fornecedor getFornecedor() {
		return fornecedor;
	}
	
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	
	public Calendar getDate() {
		return date;
	}
	
	public void setDate(Calendar date) {
		this.date = date;
	}
	
	public Cliente getCliente() {
		return cliente;
	}

	public List<ItemPedido> getItens() {
		
		List<ItemPedido> itensFinal = new ArrayList<ItemPedido>();
		
		total = 0;
		
		for (ItemPedido itemPedido : itens) {
			if(itemPedido.getQuantidade() > BigDecimal.ZERO.intValue()){
				itensFinal.add(itemPedido);
				
				total+= itemPedido.calcularTotal();
			}
		}
		
		this.itens = itensFinal;
		
		return this.itens;
	}

	public void addItem(ItemPedido item) {
		
		int index = itens.lastIndexOf(item);
		
		if(index > -1){
			ItemPedido existente = itens.get(index);
			item.somar(existente.getQuantidade());
			itens.remove(index);
		}
		
		itens.add(item);
		
		total+= item.calcularTotal();
	}
	
	public void sincItens(){
		itens = ItemPedido.find(ItemPedido.class, "PEDIDO = ?", this.getId().toString());
	}

	public boolean isSincronizado() {
		return isSincronizado;
	}

	public void setSincronizado(boolean isSincronizado) {
		this.isSincronizado = isSincronizado;
	}

	public Pedido cloneDeep() {
		
		Pedido clone = new Pedido();
		clone.setCliente(getCliente());
		clone.setFornecedor(getFornecedor());
		clone.setObservacoes(getObservacoes());
		clone.setAVista(isAVista());
		clone.setId(null);
		
		for(ItemPedido itemPedido : this.getItens()){
			ItemPedido itemClone = itemPedido.clone();
			itemClone.setPedido(clone);
			clone.addItem(itemClone);
		}
		
		return clone;
	}

	public void cleanItens() {
		this.itens = new ArrayList<ItemPedido>();
		this.total = 0;
	}

	public String getObservacoes() {
		return this.observacoes;
	}
	
	public void setObservacoes(String observacoes) {
		this.observacoes = observacoes;
	}

	public double getTotal() {
		return this.total;
	}
}
