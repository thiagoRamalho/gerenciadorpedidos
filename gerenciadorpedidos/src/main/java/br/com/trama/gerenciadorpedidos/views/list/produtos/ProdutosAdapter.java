package br.com.trama.gerenciadorpedidos.views.list.produtos;

import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import br.com.trama.gerenciadorpedidos.R;
import br.com.trama.gerenciadorpedidos.model.entity.Produto;
import br.com.trama.gerenciadorpedidos.util.ComponentUtil;

@SuppressLint("ViewTag")
public class ProdutosAdapter extends BaseAdapter{

	private List<Produto> produtos;
	private LayoutInflater inflater;
	private ComponentUtil componentUtil;
	
	public ProdutosAdapter(List<Produto> produtos, Context context) {
		super();
		this.produtos = produtos;
		this.inflater = LayoutInflater.from(context);
		componentUtil = new ComponentUtil();
	}

	@Override
	public int getCount() {
		return this.produtos.size();
	}

	@Override
	public Produto getItem(int position) {
		return this.produtos.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		
		TextView nome, precoVista, precoPrazo;
		View sincStatus;
		
		if (convertView == null) {
			
			convertView = inflater.inflate(R.layout.row_produto_layout, parent, false);
			
			nome  	   = (TextView)  convertView.findViewById(R.id.fornecedor_nome_id);
			precoVista = (TextView)  convertView.findViewById(R.id.produto_preco_vista_id);
			precoPrazo = (TextView)  convertView.findViewById(R.id.produto_preco_prazo_id);
			sincStatus = (View)  convertView.findViewById(R.id.status_shape);
			
			convertView.setTag(R.id.fornecedor_nome_id, nome);
			convertView.setTag(R.id.produto_preco_vista_id, precoVista);
			convertView.setTag(R.id.produto_preco_prazo_id, precoPrazo);
			convertView.setTag(R.id.status_shape, sincStatus);
		
		} else {
			nome 	   = (TextView)  convertView.getTag(R.id.fornecedor_nome_id);
			precoVista = (TextView)  convertView.getTag(R.id.produto_preco_vista_id);
			precoPrazo = (TextView)  convertView.getTag(R.id.produto_preco_prazo_id);
			sincStatus = (View)  convertView.getTag(R.id.status_shape);
		}
		
		Produto entidade = this.getItem(position);
		
		nome.setText(entidade.getNome());
		
		precoVista.setText(componentUtil.doubleToCurrency(entidade.getPrecoUnidadeVista()));
		precoPrazo.setText(componentUtil.doubleToCurrency(entidade.getPrecoUnidadePrazo()));
		
		int rId = entidade.isAtivo() ? R.color.sync_color : R.color.not_sync_color;
		
		sincStatus.setBackgroundResource(rId);
		
		return convertView;
	}
}
