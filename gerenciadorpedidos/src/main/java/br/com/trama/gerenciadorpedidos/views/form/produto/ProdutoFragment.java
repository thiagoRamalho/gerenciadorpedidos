package br.com.trama.gerenciadorpedidos.views.form.produto;

import java.util.List;

import roboguice.inject.InjectView;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import br.com.trama.gerenciadorpedidos.R;
import br.com.trama.gerenciadorpedidos.model.QueryUtil;
import br.com.trama.gerenciadorpedidos.model.dao.FornecedorDAO;
import br.com.trama.gerenciadorpedidos.model.entity.Fornecedor;
import br.com.trama.gerenciadorpedidos.model.entity.Produto;
import br.com.trama.gerenciadorpedidos.util.ConstantsUtil;
import br.com.trama.gerenciadorpedidos.views.form.FormFragmentViewPagerTemplate;
import br.com.trama.maskinput.CurrencyTextWatcher;

import com.google.inject.Inject;

public class ProdutoFragment extends FormFragmentViewPagerTemplate<Produto>{

	private final String TAG = ProdutoFragment.class.getSimpleName();
	
	@InjectView(R.id.produto_nome_edit)
	private EditText nome;

	@InjectView(R.id.produto_preco_vista_edit)
	private EditText precoVista;
	
	@InjectView(R.id.produto_preco_prazo_edit)
	private EditText precoPrazo;
	
	@InjectView(R.id.codigo_edit)
	private EditText codigo;

	@InjectView(R.id.ativo_edit)
	private CheckBox ativo;
	
	@InjectView(R.id.produto_fornecedor_edit)
	private Spinner fornecedorSpinner;
	
	@Inject
	private FornecedorDAO fornecedorDAO;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		return inflater.inflate(R.layout.produto_form_layout, container, false);
	}
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		
		super.onViewCreated(view, savedInstanceState);
		
		Log.d(TAG, "onViewCreated");
		
		configComponents();
		
		populateForm(savedInstanceState);
	}

	private void populateForm(Bundle savedInstanceState) {
		
		Log.d(TAG, "populateForm");
		
		getEntity(savedInstanceState);
		
		nome.setText(super.entity.getNome());
		
		codigo.setText(super.entity.getCodigo());
		
		ativo.setChecked(super.entity.isAtivo());
		
		precoVista.setText(this.componentUtil.doubleToCurrency(super.entity.getPrecoUnidadeVista()));
		precoPrazo.setText(this.componentUtil.doubleToCurrency(super.entity.getPrecoUnidadePrazo()));
		
		Fornecedor selectedFornec = super.entity.getFornecedor();
		
		@SuppressWarnings("unchecked")
		ArrayAdapter<Fornecedor> adapter = (ArrayAdapter<Fornecedor>) fornecedorSpinner.getAdapter();
		
		int position = adapter.getPosition(selectedFornec);
		
		fornecedorSpinner.setSelection(position);
	}


	private void configComponents() {
		
		Log.d(TAG, "configComponents");
		
		precoVista.addTextChangedListener(new CurrencyTextWatcher());
		precoVista.setRawInputType(InputType.TYPE_CLASS_NUMBER);
		
		precoPrazo.addTextChangedListener(new CurrencyTextWatcher());
		precoPrazo.setRawInputType(InputType.TYPE_CLASS_NUMBER);
		
		List<Fornecedor> find = this.fornecedorDAO.findByAtivo(true);
		
		ArrayAdapter<Fornecedor> arrayAdapter = 
		new ArrayAdapter<Fornecedor>(getActivity(), android.R.layout.simple_spinner_item, find);
		
		arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		
		fornecedorSpinner.setAdapter(arrayAdapter);
	}


	protected void populateEntity() {
		
		super.entity.setNome(super.componentUtil.extractValue(nome));

		double precoUnidadeVista = super.componentUtil.currencyToDouble(precoVista.getText().toString());
		super.entity.setPrecoUnidadeVista(precoUnidadeVista);
		
		double precoUnidadePrazo = super.componentUtil.currencyToDouble(precoPrazo.getText().toString());
		super.entity.setPrecoUnidadePrazo(precoUnidadePrazo);
		
		super.entity.setCodigo(super.componentUtil.extractValue(codigo));
		
		super.entity.setAtivo(ativo.isChecked());
		
		Fornecedor selectedItem = (Fornecedor) fornecedorSpinner.getSelectedItem();
		
		super.entity.setFornecedor(selectedItem);
	}

	protected boolean validate() {
		
		boolean isOk = true;
		
		if(!validateNome()){
			isOk = false;
		}

		if(!validatePreco(precoVista)){
			isOk = false;
		}
		
		if(!validatePreco(precoPrazo)){
			isOk = false;
		}
		
		if(!validateCodigo()){
			isOk = false;
		}
		
		return isOk;
	}

	private boolean validatePreco(EditText preco) {
		
		boolean isOk = true;
		
		if(!isMinNumberValue(preco)){
			preco.setError(invalidValue);
			isOk = false;
		}
		
		return isOk;
	}

	private boolean isMinNumberValue(EditText component) {
		
		boolean isOk = true;
		
		try{
			
			double value = Double.valueOf(super.componentUtil.extractValue(component, ConstantsUtil.DENIEL_NUMBERS_REGEX));
			
			isOk = value > ConstantsUtil.MIN_VALUE_NUMBER;
			
		}catch(NumberFormatException e){
			isOk = false;
		}
		
		return isOk;
	}

	private boolean validateNome() {
		
		boolean isOk = true;
		
		if(super.componentUtil.isEmpty(nome)){
			nome.setError(invalidValue);
			isOk = false;
		}
		else if(isDuplicateValue(nome, QueryUtil.BY_NOME)){
			nome.setError(duplicateValue);
			isOk = false;
		}
		
		return isOk;
	}

	private boolean validateCodigo() {
		
		boolean isOk = true;
		
		if(super.componentUtil.isEmpty(codigo)){
			codigo.setError(invalidValue);
			isOk = false;
		}
		else if(isDuplicateValue(codigo, QueryUtil.BY_CODIGO)){
			codigo.setError(duplicateValue);
			isOk = false;
		}
		
		return isOk;
	}
	
	private boolean isDuplicateValue(TextView searchValue, String where, String... replaceRegex) {
		
		boolean isOk = false;
		
		String extractValue = super.componentUtil.extractValue(searchValue, replaceRegex);
		
		List<Produto> result = Produto.find(Produto.class, where, extractValue);
		
		for (Produto produto : result) {
			
			if(!produto.getId().equals(super.entity.getId())){
				isOk = true;
				break;
			}
		}
		
		return isOk;
	}
}
