package br.com.trama.gerenciadorpedidos.views.list.clientes;

import java.util.ArrayList;
import java.util.List;

import android.text.TextUtils;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.View;
import br.com.trama.gerenciadorpedidos.R;
import br.com.trama.gerenciadorpedidos.model.QueryUtil;
import br.com.trama.gerenciadorpedidos.model.entity.Carrinho;
import br.com.trama.gerenciadorpedidos.model.entity.Cliente;
import br.com.trama.gerenciadorpedidos.views.form.FormManagerFragmentActivity;
import br.com.trama.gerenciadorpedidos.views.form.pedido.FormPedidoFragmentActivity;
import br.com.trama.gerenciadorpedidos.views.list.CustomSearchViewListFragment;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;

public class ClientesListFragment extends CustomSearchViewListFragment<Cliente> {
	
	private final String TAG = ClientesListFragment.class.getSimpleName();
	
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);
		
		super.mSearchView.setQueryHint(getString(R.string.search_cliente_hint));
	}

	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		Log.d(TAG, "onOptionsItemSelected ["+item.getItemId()+"]");
		
		if(item.getItemId() == R.id.add_id_menu_action_bar){
			runTo(FormManagerFragmentActivity.class, Cliente.class, null);
		}
		
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onResume() {
		
		Log.d(TAG, "onResume");
		
		List<Cliente> findAll = Cliente.listAll(Cliente.class);

		setData(findAll);
		
		super.onResume();
	}
	
	private void setData(List<Cliente> findAll) {
		
		ClientesAdapter clientesAdapter = new ClientesAdapter(findAll, getActivity());
		
		listView.setAdapter(clientesAdapter);
	}

	
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {

		getActivity().getMenuInflater().inflate(R.menu.context_menu, menu);

		super.onCreateContextMenu(menu, v, menuInfo);
	}
	
	@Override
	public boolean onContextItemSelected(android.view.MenuItem item) {

		switch (item.getItemId()) {
		case R.id.edit_context_menu_id:
			runTo(FormManagerFragmentActivity.class, Cliente.class, this.entity.getId());
			break;
		
		case R.id.add_context_menu_id:
			Carrinho.addCliente(this.entity);
			runTo(FormPedidoFragmentActivity.class, Cliente.class, this.entity.getId());
			break;
			
		default:
			break;
		}

		return true;
	}
	
	@Override
	public boolean onQueryTextChange(String arg0) {
		
		Log.d(TAG, "onQueryTextChange ["+arg0+"]");
		
		List<Cliente> find = new ArrayList<Cliente>();
		
		if(!TextUtils.isEmpty(arg0)){
			
			find = Cliente.find(Cliente.class, QueryUtil.BY_NOME_FANTASIA, QueryUtil.like(arg0));
		}
		else {
			find = Cliente.listAll(Cliente.class);
		}
		
		setData(find);
		
		return true;
	}
}


