package br.com.trama.gerenciadorpedidos.views.form.clientes;

import java.util.List;

import android.widget.TextView;
import br.com.trama.gerenciadorpedidos.model.entity.Cliente;
import br.com.trama.gerenciadorpedidos.views.form.FormFragmentViewPagerTemplate;

public abstract class ClienteFormTemplate extends FormFragmentViewPagerTemplate<Cliente>{
	
	protected boolean isDuplicateValue(TextView searchValue, String where, String... replaceRegex) {
		
		boolean isOk = false;
		
		String extractValue = super.componentUtil.extractValue(searchValue, replaceRegex);
		
		List<Cliente> result = Cliente.find(Cliente.class, where, extractValue);
		
		for (Cliente cliente : result) {
			
			if(!cliente.getId().equals(super.entity.getId())){
				isOk = true;
				break;
			}
		}
		
		return isOk;
	}

}
