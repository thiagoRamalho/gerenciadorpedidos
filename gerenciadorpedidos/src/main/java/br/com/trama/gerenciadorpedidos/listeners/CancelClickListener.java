package br.com.trama.gerenciadorpedidos.listeners;

import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;

public class CancelClickListener implements OnClickListener {

	@Override
	public void onClick(DialogInterface dialog, int which) {
		dialog.cancel();
	}
}
