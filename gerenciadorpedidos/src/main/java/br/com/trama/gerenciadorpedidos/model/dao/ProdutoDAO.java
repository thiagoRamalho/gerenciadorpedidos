package br.com.trama.gerenciadorpedidos.model.dao;

import java.util.ArrayList;
import java.util.List;

import br.com.trama.gerenciadorpedidos.model.QueryUtil;
import br.com.trama.gerenciadorpedidos.model.entity.Produto;

public class ProdutoDAO {

	public void save(Produto produto){
		produto.save();
	}
	
	public List<Produto> findByAtivo(boolean isAtivo){
		
		String[] filter = {isAtivo ? "1" : "0"};
		
		return Produto.find(Produto.class, QueryUtil.BY_ATIVO, filter);
	}

	public List<Produto> findAll() {
		return Produto.listAll(Produto.class);
	}

	public List<Produto> findByNome(String text) {
		
		List<Produto> produtos = new ArrayList<Produto>();
		
		if(isTextOk(text)){
			produtos = Produto.find(Produto.class, QueryUtil.BY_NOME, QueryUtil.like(text));
		}
		
		return produtos;
		
	}

	public long count(boolean isAtivo) {
		String[] filter = {isAtivo ? "1" : "0"};
		return Produto.count(Produto.class, QueryUtil.BY_ATIVO, filter);
	}

	public List<Produto> findByNomeAndAtivo(String text, boolean isAtivo) {
		
		String[] filter = {QueryUtil.like(text), isAtivo ? "1" : "0"};
		
		List<Produto> produtos = new ArrayList<Produto>();
		
		if(isTextOk(text)){
			produtos = Produto.find(Produto.class, QueryUtil.BY_NOME_AND_ATIVO, filter);
		}
		
		return produtos;
	}

	private boolean isTextOk(String text) {
		return text != null && text.trim().length() > 0;
	}

	public List<Produto> findByFornecedor(Long idFornecedor) {
		
		String[] filter = {""+idFornecedor.longValue()};
		
		return Produto.find(Produto.class, QueryUtil.BY_FORNECEDOR, filter);
	}

	public List<Produto> findByNomeAndFornecedor(String text, boolean isAtivo, Long idFornecedor) {
		
		String[] filter = {QueryUtil.like(text), ""+idFornecedor.longValue()};
		
		return Produto.find(Produto.class, QueryUtil.BY_NOME_AND_FORNECEDOR, filter);
	}
	
	public Produto findById(long id){
		return Produto.findById(Produto.class, id);
	}
}
