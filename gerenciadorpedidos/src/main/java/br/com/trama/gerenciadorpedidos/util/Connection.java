package br.com.trama.gerenciadorpedidos.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class Connection {

	public static boolean isConnection(Context context) {
		
		boolean isConnected = false;
		
		ConnectivityManager conectivtyManager = getConectionManager(context);
		
		NetworkInfo activeNetworkInfo = conectivtyManager.getActiveNetworkInfo();
		
		if (activeNetworkInfo != null) {
			isConnected = (activeNetworkInfo.isAvailable() && activeNetworkInfo.isConnected());
		}
		
		return isConnected;
	}
	
	private static ConnectivityManager getConectionManager(Context context) {
		
		ConnectivityManager conectivtyManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		
		return conectivtyManager;
	}
}
