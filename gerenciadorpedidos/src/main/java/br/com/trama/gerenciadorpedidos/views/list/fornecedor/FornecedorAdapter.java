package br.com.trama.gerenciadorpedidos.views.list.fornecedor;

import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import br.com.trama.gerenciadorpedidos.R;
import br.com.trama.gerenciadorpedidos.model.entity.Fornecedor;

@SuppressLint("ViewTag")
public class FornecedorAdapter extends BaseAdapter{

	private List<Fornecedor> fornecedores;
	private LayoutInflater inflater;
	
	public FornecedorAdapter(List<Fornecedor> fornecedores, Context context) {
		super();
		this.fornecedores = fornecedores;
		this.inflater = LayoutInflater.from(context);
	}

	@Override
	public int getCount() {
		return this.fornecedores.size();
	}

	@Override
	public Fornecedor getItem(int position) {
		return this.fornecedores.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		
		TextView nome, linha;
		View sincStatus;
		
		if (convertView == null) {
			
			convertView = inflater.inflate(R.layout.row_fornecedor_layout, parent, false);
			
			nome  	   = (TextView)  convertView.findViewById(R.id.fornecedor_nome_id);
			linha 	   = (TextView)  convertView.findViewById(R.id.fornecedor_linha_prod_id);
			sincStatus = (View)  convertView.findViewById(R.id.status_shape);
			
			convertView.setTag(R.id.fornecedor_nome_id, nome);
			convertView.setTag(R.id.fornecedor_linha_prod_id, linha);
			convertView.setTag(R.id.status_shape, sincStatus);
		
		} else {
			nome 	   = (TextView)  convertView.getTag(R.id.fornecedor_nome_id);
			linha 	   = (TextView)  convertView.getTag(R.id.fornecedor_linha_prod_id);
			sincStatus = (View)  convertView.getTag(R.id.status_shape);
		}
		
		Fornecedor entidade = this.getItem(position);
		
		nome.setText(entidade.getNome());
		
		linha.setText(entidade.getLinhaProdutos());
		
		int rId = entidade.isAtivo() ? R.color.sync_color : R.color.not_sync_color;
		
		sincStatus.setBackgroundResource(rId);
		
		return convertView;
	}
}
