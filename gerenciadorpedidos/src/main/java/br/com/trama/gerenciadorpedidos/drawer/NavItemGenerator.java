package br.com.trama.gerenciadorpedidos.drawer;

import java.util.ArrayList;
import java.util.List;

import android.content.res.TypedArray;

public class NavItemGenerator {


	public List<NavDrawerItem> execute(String[] navMenuTitles, TypedArray navMenuIcons){

		ArrayList<NavDrawerItem> navDrawerItems = new ArrayList<NavDrawerItem>();

		for (int i = 0; i < navMenuTitles.length; i++) {
			
			String title = navMenuTitles[i];
			
			int resourceIdIcon = navMenuIcons.getResourceId(i, -1);
			
			NavDrawerItem n = new NavDrawerItem(title, resourceIdIcon);
			
			navDrawerItems.add(n);
		}
		
		return navDrawerItems;
	}
}
