package br.com.trama.gerenciadorpedidos.views.list.clientes;

import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import br.com.trama.gerenciadorpedidos.R;
import br.com.trama.gerenciadorpedidos.model.entity.Cliente;
import br.com.trama.gerenciadorpedidos.model.entity.Endereco;

@SuppressLint("ViewTag")
public class ClientesAdapter extends BaseAdapter{

	private List<Cliente> clientes;
	private LayoutInflater inflater;
	
	public ClientesAdapter(List<Cliente> clientes, Context context) {
		super();
		this.clientes = clientes;
		this.inflater = LayoutInflater.from(context);
	}

	@Override
	public int getCount() {
		return this.clientes.size();
	}

	@Override
	public Cliente getItem(int position) {
		return this.clientes.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		
		TextView nomeFantasia, bairro, cidade;
		
		if (convertView == null) {
			
			convertView = inflater.inflate(R.layout.row_cliente_layout, parent, false);
			
			nomeFantasia = (TextView)  convertView.findViewById(R.id.cliente_nome_fantasia_id);
			bairro = (TextView)  convertView.findViewById(R.id.cliente_bairro_id);
			cidade = (TextView)  convertView.findViewById(R.id.cliente_cidade_id);
			
			convertView.setTag(R.id.cliente_nome_fantasia_id, nomeFantasia);
			convertView.setTag(R.id.cliente_bairro_id, bairro);
			convertView.setTag(R.id.cliente_cidade_id, cidade);
		
		} else {
			nomeFantasia = (TextView)  convertView.getTag(R.id.cliente_nome_fantasia_id);
			bairro = (TextView)  convertView.getTag(R.id.cliente_bairro_id);
			cidade = (TextView)  convertView.getTag(R.id.cliente_cidade_id);
		}
		
		Cliente entidade = this.getItem(position);
		Endereco endereco = entidade.getEndereco();
		
		nomeFantasia.setText(entidade.getNomeFantasia());
		bairro.setText(endereco.getBairro());
		cidade.setText(endereco.getCidade());
		
		return convertView;
	}
	

	
}
