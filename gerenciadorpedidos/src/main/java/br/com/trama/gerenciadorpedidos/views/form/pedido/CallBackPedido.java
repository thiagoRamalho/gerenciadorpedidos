package br.com.trama.gerenciadorpedidos.views.form.pedido;

public interface CallBackPedido {

	void adicionarClienteAoCarrinho();

	void adicionarItemAoCarrinho();

	void adicionarFornecedorAoCarrinho();
}
