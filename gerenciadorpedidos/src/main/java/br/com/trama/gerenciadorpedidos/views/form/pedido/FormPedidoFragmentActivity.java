package br.com.trama.gerenciadorpedidos.views.form.pedido;

import roboguice.inject.ContentView;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import br.com.trama.gerenciadorpedidos.MainActivity;
import br.com.trama.gerenciadorpedidos.R;
import br.com.trama.gerenciadorpedidos.listeners.CancelClickListener;
import br.com.trama.gerenciadorpedidos.model.entity.Carrinho;
import br.com.trama.gerenciadorpedidos.model.entity.Fornecedor;
import br.com.trama.gerenciadorpedidos.util.ConstantsUtil;
import br.com.trama.gerenciadorpedidos.util.RouterUtil;
import br.com.trama.gerenciadorpedidos.views.form.fornecedor.OperationType;
import br.com.trama.gerenciadorpedidos.views.list.CustomSearchViewListFragment;
import br.com.trama.gerenciadorpedidos.views.list.clientes.ClientesListFragment;
import br.com.trama.gerenciadorpedidos.views.list.fornecedor.FornecedorListFragment;
import br.com.trama.gerenciadorpedidos.views.list.produtos.ProdutosListFragment;

import com.actionbarsherlock.view.MenuItem;
import com.github.rtyley.android.sherlock.roboguice.activity.RoboSherlockFragmentActivity;
import com.google.inject.Inject;

@ContentView(R.layout.fragment_container_layout)
public class FormPedidoFragmentActivity extends RoboSherlockFragmentActivity implements CallBackPedido{

	@Inject
	private RouterUtil routerUtil;

	private AlertDialog dialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		AlertDialog.Builder builder = new AlertDialog.Builder(this);

		builder.setNegativeButton(android.R.string.cancel, new CancelClickListener());

		builder.setPositiveButton(android.R.string.ok, new ChangeFornecedorClickListener());

		builder.setTitle(R.string.atencao);

		builder.setMessage(R.string.msg_change_fornecedor);

		dialog = builder.create();

		PedidoFragment pedidoFragment = new PedidoFragment();
		pedidoFragment.setArguments(getIntent().getExtras());

		this.routerUtil.routeFragment(this, R.id.fragment_container, pedidoFragment);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		// Respond to the action bar's Up/Home button
		case android.R.id.home:
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}

		return super.onOptionsItemSelected(item);
	}
	
	@Override
	public void onBackPressed() {
		routerUtil
		.withClass(MainActivity.class)
		.withContext(this)
		.route();
		finish();
	}

	private void configListFragment(CustomSearchViewListFragment<?> customSearchViewListFragment, int rIdTtile) {

		Fornecedor fornecedor = Carrinho.getPedido().getFornecedor();

		Bundle args = new Bundle();
		args.putString(ConstantsUtil.TITLE_FRAGMENT, getString(rIdTtile));

		if(fornecedor!=null){
			args.putString(ConstantsUtil.SUBTITLE_FRAGMENT, fornecedor.getNome());
		}

		customSearchViewListFragment.setArguments(args);
	}

	private void runFornecedor() {

		FornecedorListFragment fornecedorListFragment = new FornecedorListFragment();
		fornecedorListFragment.setOperation(OperationType.ALL);

		configListFragment(fornecedorListFragment, R.string.selecionar_fornecedor);

		routerUtil.routeFragment(this, R.id.fragment_container, fornecedorListFragment);
	}


	@Override
	public void adicionarClienteAoCarrinho() {

		ClientesListFragment clientesListFragment = new ClientesListFragment();

		Bundle args = new Bundle();
		args.putString(ConstantsUtil.TITLE_FRAGMENT, getString(R.string.selecionar_cliente));
		clientesListFragment.setArguments(args);

		this.routerUtil.routeFragment(this, R.id.fragment_container, clientesListFragment);
	}

	@Override
	public void adicionarFornecedorAoCarrinho() {

		Fornecedor fornecedor = Carrinho.getPedido().getFornecedor();

		if(fornecedor != null){
			dialog.show();
		} else {
			runFornecedor();
		}
	}

	@Override
	public void adicionarItemAoCarrinho() {

		ProdutosListFragment produtosListFragment = new ProdutosListFragment();
		produtosListFragment.setOperation(OperationType.ADD_PRODUTO_PEDIDO);

		configListFragment(produtosListFragment, R.string.selecionar_produtos);

		this.routerUtil.routeFragment(this, R.id.fragment_container, produtosListFragment);
	}

	class ChangeFornecedorClickListener implements OnClickListener{

		@Override
		public void onClick(DialogInterface dialog, int which) {
			runFornecedor();
		}
	}
}
