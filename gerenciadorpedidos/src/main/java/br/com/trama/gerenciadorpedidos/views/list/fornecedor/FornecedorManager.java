package br.com.trama.gerenciadorpedidos.views.list.fornecedor;

import java.math.BigDecimal;

import android.app.AlertDialog;
import android.content.Context;
import br.com.trama.gerenciadorpedidos.R;
import br.com.trama.gerenciadorpedidos.listeners.CancelClickListener;
import br.com.trama.gerenciadorpedidos.model.dao.FornecedorDAO;

import com.google.inject.Inject;

public class FornecedorManager {

	@Inject
	FornecedorDAO fornecedorDAO;
	
	public boolean possui(Context context){
		
		boolean isOk = true;
		
		if(this.fornecedorDAO.count(true) < BigDecimal.ONE.longValue()){
			
			isOk = false;
			
			AlertDialog.Builder builder = new AlertDialog.Builder(context);

			builder.setNeutralButton(android.R.string.ok, new CancelClickListener());

			builder.setTitle(R.string.atencao);

			AlertDialog dialog = builder.create();
			
			dialog.setMessage(context.getString(R.string.msg_nao_existe_fornecedor));
			dialog.show();
			
		}
		
		return isOk;
	}
}
