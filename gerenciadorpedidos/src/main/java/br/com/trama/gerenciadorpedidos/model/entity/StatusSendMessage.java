package br.com.trama.gerenciadorpedidos.model.entity;

import java.io.Serializable;
import java.util.Calendar;

import com.orm.SugarRecord;

public class StatusSendMessage extends SugarRecord<StatusSendMessage> implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private String smtp;
	private int port;
	private String ex;
	private String subject;
	private Calendar date;
	
	public StatusSendMessage() {
		this.date = Calendar.getInstance();
	}
	
	public StatusSendMessage smtp(String smtp){
		this.smtp = smtp;
		return this;
	}
	
	public StatusSendMessage port(int port){
		this.port = port;
		return this;
	}

	public StatusSendMessage subject(String subject){
		this.subject = subject;
		return this;
	}
	
	public StatusSendMessage ex(String ex){
		this.ex = ex;
		return this;
	}

	public String getSubject() {
		return subject;
	}
	
	public String getSmtp() {
		return smtp;
	}

	public int getPort() {
		return port;
	}

	public String getEx() {
		return ex;
	}
	
	public Calendar getDate() {
		return date;
	}
}
