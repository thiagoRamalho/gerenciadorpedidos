package br.com.trama.gerenciadorpedidos.util;

import java.io.Serializable;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;

public class RouterUtil {

	private Class<?> clazz;
	private Context context;
	private Class<? extends IntentService> service;
	private Intent intent;

	public RouterUtil withClass(Class<?> clazz){
		this.clazz = clazz;
		return this;
	}

	public RouterUtil withContext(Context context){
		this.context = context;
		return this;
	}
	
	public RouterUtil withService(Class<? extends IntentService> service){
		this.service = service;
		return this;
	}
	
	public <T extends Serializable> RouterUtil andExtra(String key, T value){
		
		if(this.intent == null){
			this.intent = getIntent(this.clazz);
		}
		
		this.intent.putExtra(key, value);
		
		return this;
	}


	/**
	 * Inicializa a clazz enviada no construtor
	 * 
	 */
	public void route(){
		
		if(this.intent == null){
			this.intent = getIntent(clazz);
		}
		
		this.start(this.intent);
		this.clear();
	}

	/**
	 * Inicializa a clazz enviado value com 
	 * a chave key em extra
	 * 
	 * @param key
	 * @param value
	 */
	public <T extends Serializable> void routeAndExtra(String key, T value, String idKey, Long idValue){
		
		andExtra(idKey, value);
		
		if(!TextUtils.isEmpty(idKey) && idValue != null)
			intent.putExtra(idKey, idValue);
		
		this.start(intent);
		
		this.clear();
	}
	
	public <T extends Serializable> void routeAndExtra(String key, T value){
		this.routeAndExtra(key, value, null, null);
	}


	private Intent getIntent(Class<?> clazzParam) {

		if(this.context == null){
			throw new IllegalArgumentException("context don't be null");
		}

		if(clazzParam == null){
			throw new IllegalArgumentException("class don't be null");
		}
		
		return new Intent(this.context, clazzParam);
	}

	private void start(Intent intent){
		this.context.startActivity(intent);
	}

	public void routeFragment(FragmentActivity fragmentActivity, int containerViewId, Fragment fragment) {
		FragmentManager supportFragmentManager = fragmentActivity.getSupportFragmentManager();
		FragmentTransaction transaction = supportFragmentManager.beginTransaction();
		transaction.replace(containerViewId, fragment);
		transaction.addToBackStack(null);
		transaction.commit();
		this.clear();
	}
	
	public void startService(){
		Intent intent = getIntent(service);
		this.context.startService(intent);
		this.clear();
	}

	private void clear() {
		this.context = null;
		this.service = null;
		this.clazz = null;
		this.intent = null;
	}
}
