package br.com.trama.gerenciadorpedidos.validator;

import android.widget.TextView;
import br.com.trama.validators.AlgorithmValidator;
import br.com.trama.validators.cnpj.CNPJStrategy;

public class CNPJValidator {

	private final AlgorithmValidator algorithmValidator;
	
	public CNPJValidator() {
		this.algorithmValidator = new AlgorithmValidator(new CNPJStrategy());
	}

	public boolean isValid(TextView arg0) {
		return algorithmValidator.isValid(arg0.getText().toString());
	}
}
