package br.com.trama.gerenciadorpedidos.mail;

import android.content.Context;
import br.com.trama.gerenciadorpedidos.R;
import br.com.trama.gerenciadorpedidos.model.entity.Cliente;
import br.com.trama.gerenciadorpedidos.model.entity.Comunicacao;
import br.com.trama.gerenciadorpedidos.model.entity.Endereco;
import br.com.trama.gerenciadorpedidos.model.entity.Pedido;
import br.com.trama.gerenciadorpedidos.util.ConstantsUtil;
import br.com.trama.maskinput.MaskWatcher;

import com.x5.template.Chunk;
import com.x5.template.Theme;
import com.x5.template.providers.AndroidTemplates;

public class TemplateMessage {

	private final Pedido pedido;
	private final Context context;
	private String vendedor;

	public TemplateMessage(Pedido pedido, String vendedor, Context context) {
		super();
		this.pedido = pedido;
		this.vendedor = vendedor;
		this.context = context;
	}

	public String create() {

		String result = "";

		try{

			AndroidTemplates loader = new AndroidTemplates(this.context);
			Theme theme = new Theme(loader);
			Chunk chunk = theme.makeChunk("order_template");

			setInfoFiscal(chunk);

			setInfoLocalizacao(chunk);

			setInfoComunicacao(chunk);

			setInfoItens(chunk);	

			setInfoFinais(chunk);
			
			result = chunk.toString();
			
		}catch(Exception e){
			throw new RuntimeException(e);
		}

		return result;
	}

	private void setInfoFinais(Chunk chunk) {
		
		chunk.set("TOTAL", this.pedido.getTotal());
		chunk.set("OBSERVACOES", this.pedido.getObservacoes());
	}

	private void setInfoComunicacao(Chunk chunk) {

		MaskWatcher maskTel = new MaskWatcher(ConstantsUtil.MASK_TEL, ConstantsUtil.DENIEL_NUMBERS_REGEX);
		MaskWatcher maskCel = new MaskWatcher(ConstantsUtil.MASK_CEL, ConstantsUtil.DENIEL_NUMBERS_REGEX);

		Cliente cliente = this.pedido.getCliente();
		Comunicacao comunicacao = cliente.getComunicacao();

		chunk.set("CONTATO", comunicacao.getContato());

		maskTel.onTextChanged(""+comunicacao.getTelefone(), 0, 0, 0);
		chunk.set("TELEFONE", maskTel.getMaskedValue());

		maskCel.onTextChanged(""+comunicacao.getCelular(), 0, 0, 0);
		chunk.set("CELULAR", maskCel.getMaskedValue());

		chunk.set("NEXTEL", comunicacao.getNextel());
		chunk.set("EMAIL", comunicacao.getEmail());
	}

	private void setInfoLocalizacao(Chunk chunk) {

		Cliente cliente = this.pedido.getCliente();

		Endereco endereco = cliente.getEndereco();

		MaskWatcher maskWatcher = new MaskWatcher(ConstantsUtil.MASK_CEP, ConstantsUtil.DENIEL_NUMBERS_REGEX);
		maskWatcher.onTextChanged(endereco.getCep(), 0, 0, 0);

		chunk.set("LOGRADOURO", endereco.getLogradouro());
		chunk.set("NUMERO", endereco.getNumero());
		chunk.set("COMPLEMENTO", endereco.getComplemento());
		chunk.set("BAIRRO", endereco.getBairro());
		chunk.set("CIDADE", endereco.getCidade());
		chunk.set("CEP", maskWatcher.getMaskedValue());
		chunk.set("UF", endereco.getUf());
	}

	private void setInfoItens(Chunk chunk) {
		chunk.set("list", this.pedido.getItens());
	}

	private void setInfoFiscal(Chunk chunk) {

		Cliente cliente = pedido.getCliente();

		chunk.set("NUMERO_PEDIDO", this.pedido.getId().toString());
		
		chunk.set("VENDEDOR", this.vendedor);
		
		chunk.set("RAZAO_SOCIAL", cliente.getRazaoSocial());

		chunk.set("NOME_FANTASIA", cliente.getNomeFantasia());

		MaskWatcher maskIE = new MaskWatcher(ConstantsUtil.MASK_IE, ConstantsUtil.DENIEL_NUMBERS_REGEX);
		maskIE.onTextChanged(cliente.getInscricaoEstadual(), 0, 0, 0);
		chunk.set("IE", maskIE.getMaskedValue());
		
		MaskWatcher maskCNPJ = new MaskWatcher(ConstantsUtil.MASK_CNPJ, ConstantsUtil.DENIEL_NUMBERS_REGEX);
		maskCNPJ.onTextChanged(cliente.getCnpj(), 0, 0, 0);
		chunk.set("CNPJ", maskCNPJ.getMaskedValue());

		chunk.set("IM", cliente.getInscricaoMunicipal());
		
		String condicoes = context.getString(this.pedido.isAVista() ? R.string.a_vista : R.string.a_prazo);
		
		chunk.set("CONDICOES_PAGAMENTO", condicoes);
	}
}
