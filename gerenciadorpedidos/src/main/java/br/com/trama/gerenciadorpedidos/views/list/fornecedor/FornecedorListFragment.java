package br.com.trama.gerenciadorpedidos.views.list.fornecedor;

import java.util.List;

import android.text.TextUtils;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.View;
import br.com.trama.gerenciadorpedidos.R;
import br.com.trama.gerenciadorpedidos.model.dao.FornecedorDAO;
import br.com.trama.gerenciadorpedidos.model.entity.Carrinho;
import br.com.trama.gerenciadorpedidos.model.entity.Fornecedor;
import br.com.trama.gerenciadorpedidos.views.form.FormManagerFragmentActivity;
import br.com.trama.gerenciadorpedidos.views.form.pedido.FormPedidoFragmentActivity;
import br.com.trama.gerenciadorpedidos.views.list.CustomSearchViewListFragment;
import br.com.trama.gerenciadorpedidos.views.list.SearchStrategy;

import com.actionbarsherlock.view.MenuItem;
import com.google.inject.Inject;

public class FornecedorListFragment extends CustomSearchViewListFragment<Fornecedor>{

	private final String TAG = FornecedorListFragment.class.getSimpleName();
	
	@Inject
	private FornecedorDAO fornecedorDAO;

	private SearchStrategy<Fornecedor> searchStrategy;

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {

		getActivity().getMenuInflater().inflate(R.menu.context_menu, menu);

		android.view.MenuItem item = menu.findItem(R.id.add_context_menu_id);
		
		if(!super.entity.isAtivo()){
			menu.removeItem(item.getItemId());
		}
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		Log.d(TAG, "onOptionsItemSelected ["+item.getItemId()+"]");

		if(item.getItemId() == R.id.add_id_menu_action_bar){
			runTo(FormManagerFragmentActivity.class, Fornecedor.class, null);
		}

		return super.onOptionsItemSelected(item);
	}


	@Override
	public void onResume() {
		
		Log.d(TAG, "onResume");
		
		searchStrategy = new SearchAll(fornecedorDAO);
		
/*		if(OperationType.ONLY_ACTIVE.equals(operationType)){
			searchStrategy = new SearchAtivo(fornecedorDAO);
		}
*/		
		setData(searchStrategy.search());

		
		super.onResume();
	}

	private void setData(List<Fornecedor> findAll) {
		FornecedorAdapter fornecedorAdapter = new FornecedorAdapter(findAll, getActivity());
		listView.setAdapter(fornecedorAdapter);
	}

	@Override
	public boolean onQueryTextSubmit(String text) {
		return this.onQueryTextChange(text);
	}

	@Override
	public boolean onQueryTextChange(String text) {
		
		boolean isChange = false;
		
		Log.d(TAG, "onQueryTextChange ["+text+"]");

		if(!TextUtils.isEmpty(text)){
			
			setData(this.searchStrategy.searchBy(text));
			
			isChange = true;
		}

		return isChange;
	}


	@Override
	public boolean onContextItemSelected(android.view.MenuItem item) {

		Log.d(TAG, "onContextItemSelected ["+item.getItemId()+"]");

		switch (item.getItemId()) {
		case R.id.edit_context_menu_id:
			runTo(FormManagerFragmentActivity.class, Fornecedor.class, this.entity.getId());
			break;

		case R.id.add_context_menu_id:
			Carrinho.addFornecedor(this.entity);
			runTo(FormPedidoFragmentActivity.class, Fornecedor.class, this.entity.getId());
			break;

		default:
			break;
		}

		return true;
	}
	
	@Override
	public boolean onMenuItemActionCollapse(MenuItem item) {
		
		Log.d(TAG, "onMenuItemActionCollapse");
		
		mSearchView.setQuery("", false);
		
		setData(searchStrategy.search());
		
		return true;
	}

}
