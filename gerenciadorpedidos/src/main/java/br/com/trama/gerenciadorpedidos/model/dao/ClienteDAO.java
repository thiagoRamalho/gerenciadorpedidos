package br.com.trama.gerenciadorpedidos.model.dao;

import java.util.ArrayList;
import java.util.List;

import br.com.trama.gerenciadorpedidos.model.entity.Cliente;

public class ClienteDAO {

	private List<Cliente> clientes;
	
	public ClienteDAO() {
		
		clientes = new ArrayList<Cliente>();
	}
	
	public List<Cliente> findAll(){
		return clientes;
	}
	
	public List<Cliente> findByText(String text){
		
		List<Cliente> clientesResult = new ArrayList<Cliente>();

		for (Cliente cliente : clientes) {
			
			if(cliente.getRazaoSocial().contains(text)){
				clientesResult.add(cliente);
			}
		}
		
		return clientesResult;
	}
}
