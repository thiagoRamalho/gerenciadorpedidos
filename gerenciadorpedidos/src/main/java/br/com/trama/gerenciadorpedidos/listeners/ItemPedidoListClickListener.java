package br.com.trama.gerenciadorpedidos.listeners;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.text.InputType;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import br.com.trama.gerenciadorpedidos.R;
import br.com.trama.gerenciadorpedidos.model.entity.ItemPedido;
import br.com.trama.gerenciadorpedidos.views.form.pedido.PedidoFragment;

public class ItemPedidoListClickListener implements OnItemClickListener{

	private ItemPedido itemPedido;
	private EditText quantidadeEditText;
	private PedidoFragment pedidoFragment;
	
	public ItemPedidoListClickListener(PedidoFragment pedidoFragment) {
		super();
		this.pedidoFragment = pedidoFragment;
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		
		itemPedido = (ItemPedido) parent.getItemAtPosition(position);
		
		quantidadeEditText = new EditText(view.getContext());
		
		quantidadeEditText.setInputType(InputType.TYPE_CLASS_NUMBER);
		quantidadeEditText.setText(""+itemPedido.getQuantidade());
		
		Builder builder = createBuilder(view.getContext());
		
		builder.setTitle(itemPedido.getNome());
		builder.setMessage(R.string.msg_determine_quantidade);
		
		AlertDialog dialog = builder.create();
		dialog.setView(quantidadeEditText);
		quantidadeEditText.requestFocus();
		dialog.show();
	}
	
	private Builder createBuilder(Context context) {
		
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		
		builder.setPositiveButton(android.R.string.ok, new PositiveClickListener());
		
		builder.setNegativeButton(android.R.string.cancel, new CancelClickListener());
		
		return builder;
	}

	class PositiveClickListener implements DialogInterface.OnClickListener{
            
		public void onClick(DialogInterface dialog, int id) {
            	
            	int quantidade = 0;
            	
            	String strQuantidade = quantidadeEditText.getText().toString();
            	
            	if(!TextUtils.isEmpty(strQuantidade)){
            		quantidade = Integer.valueOf(strQuantidade);
            	}
            	
            	itemPedido.zerarQuantidade();
            	itemPedido.somar(quantidade);
            	
            	pedidoFragment.updateItens();
            	
            	dialog.cancel();
            }
        }
	}
