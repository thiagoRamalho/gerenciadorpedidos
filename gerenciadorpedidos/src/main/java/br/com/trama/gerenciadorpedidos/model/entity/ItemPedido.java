package br.com.trama.gerenciadorpedidos.model.entity;

import com.orm.SugarRecord;

public class ItemPedido extends SugarRecord<ItemPedido> implements java.io.Serializable{

	private static final long serialVersionUID = 1L;

	int quantidade;
	
	long produto;
	
	Double preco;

	Pedido pedido;
	
	String nome;
	
	public ItemPedido(){}
	
	public ItemPedido(Pedido pedido, long produtoId, double preco, int quantidade) {
		super();
		this.pedido = pedido;
		this.produto = produtoId;
		this.quantidade = quantidade;
		this.preco = preco;
	}
	
	
	public int getQuantidade() {
		return quantidade;
	}
	
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}

	public void zerarQuantidade(){
		this.quantidade = 0;
	}
	
	public void somar(int quantidade) {
		this.quantidade+= quantidade;
	}
	
	public double calcularTotal(){
		return preco * quantidade;
	}
	
	public double getPreco() {
		return preco; 
	}
	
	public void setPreco(Double preco) {
		this.preco = preco;
	}
	
	public long getProdutoId() {
		return produto;
	}
	
	@Override
	public ItemPedido clone()  {
		
		ItemPedido item = new ItemPedido(pedido, produto, preco, quantidade);
		item.setNome(nome);
		
		return item;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (produto ^ (produto >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ItemPedido other = (ItemPedido) obj;
		if (produto != other.produto)
			return false;
		return true;
	}

	public void setPedido(Pedido pedido) {
		this.pedido = pedido;
	}

	@Override
	public String toString() {
		return "ItemPedido [quantidade=" + quantidade + ", produto=" + produto
				+ ", preco=" + preco + ", nome=" + nome + "]";
	}
	
	
}
