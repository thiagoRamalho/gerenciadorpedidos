package br.com.trama.gerenciadorpedidos.mail;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import android.content.Context;
import android.content.res.AssetManager;
import android.text.TextUtils;
import android.util.Log;
import br.com.trama.gerenciadorpedidos.model.entity.StatusSendMessage;
import br.com.trama.gerenciadorpedidos.util.ConstantsUtil;

public class SendMailJavaMailBuilder implements SendMailBuilder{

	private final String TAG = SendMailJavaMailBuilder.class.getSimpleName();

	private String userName;
	private String password;
	private String from;
	private List<String> cc;
	private String subject;
	private String messageBody;
	private String smptHost;
	private int port;

	private MimeMultipart multipart;

	private File file;

	private Context context;

	private List<StatusSendMessage> statusSendMessage;

	public SendMailJavaMailBuilder(String smptHost, int port, Context context) {
		super();
		this.smptHost = smptHost;
		this.port = port;
		this.context = context;
		this.cc = new ArrayList<String>();
		this.statusSendMessage = new ArrayList<StatusSendMessage>();
	}

	@Override
	public SendMailBuilder auth(String userName, String password) {
		this.userName = userName;
		this.password = password;
		return this;
	}

	@Override
	public SendMailBuilder from(String from) {
		this.from = from;
		return this;
	}

	@Override
	public SendMailBuilder to(String to) {
		return this.cc(to);
	}

	@Override
	public SendMailBuilder cc(String cc) {
		if(!TextUtils.isEmpty(cc)){
			this.cc.add(cc);
		}
		return this;
	}

	@Override
	public SendMailBuilder subject(String subject) {
		this.subject = subject;
		return this;
	}

	@Override
	public boolean send(){

		boolean isSend = false;

		try{

			Properties props = this.loadProperties();

			props.setProperty(ConstantsUtil.KEY_PROP_MAIL_SMTP_URL, this.smptHost);
			props.setProperty(ConstantsUtil.KEY_PROP_MAIL_SMTP_PORT, ""+this.port);

			Session session = Session.getInstance(props, new PasswordAuthenticator(userName, password));

			MimeMessage message = new MimeMessage(session);
			message.setFrom(new InternetAddress(this.from));

			for (String copy: this.cc) {
				message.addRecipient(Message.RecipientType.TO, new InternetAddress(copy));
			}

			message.setSubject(subject);
			this.multipart = new MimeMultipart();

			MimeBodyPart messageBodyPart = new MimeBodyPart();
			messageBodyPart.setText(this.messageBody); 

			this.multipart.addBodyPart(messageBodyPart);

			configAttachment();

			message.setContent(multipart);
			message.setSentDate(new Date());

			Transport.send(message);

			isSend = true;
			
		}catch(Exception e){
			StatusSendMessage configStatusMessage = configStatusMessage(e);
			isSend = false;
			Log.e(TAG, configStatusMessage.getEx());

		}finally{
			this.cc.clear();
		}

		return isSend;
	}

	private StatusSendMessage configStatusMessage(Exception e) {
		
		String msg = e.getMessage();
		
		StatusSendMessage statusSendMessage = new StatusSendMessage();
		statusSendMessage
		.smtp(this.smptHost)
		.port(port)
		.subject(this.subject)
		.ex(TextUtils.isEmpty(msg) ? e.getClass().getCanonicalName() : msg);
		this.statusSendMessage.add(statusSendMessage);
		
		return statusSendMessage;
	}

	private boolean configAttachment() throws MessagingException {

		boolean isAttach = false;

		if(file != null && file.isFile()){
			isAttach = true;

			BodyPart messageBodyPart = new MimeBodyPart(); 
			DataSource source = new FileDataSource(file); 

			messageBodyPart.setDataHandler(new DataHandler(source)); 
			messageBodyPart.setFileName(file.getName()); 

			multipart.addBodyPart(messageBodyPart); 
		}

		return isAttach;
	}

	@Override
	public SendMailBuilder withMessageBody(String messageBody) {
		this.messageBody = messageBody;
		return this;
	}

	@Override
	public SendMailBuilder withAttach(File file) {
		this.file = file;
		return this;
	}

	public List<StatusSendMessage> getStatusSendMessage() {
		return statusSendMessage;
	}
	
	private Properties loadProperties(){

		Properties properties = new Properties();

		try {

			AssetManager assets = this.context.getAssets();

			InputStream inputStream = assets.open(ConstantsUtil.PROP_MAIL_FILE);
			properties.load(inputStream);

		} catch (IOException e) {
			Log.e(TAG, e.getMessage());
			throw new RuntimeException(e);
		}

		return properties;
	}
}
