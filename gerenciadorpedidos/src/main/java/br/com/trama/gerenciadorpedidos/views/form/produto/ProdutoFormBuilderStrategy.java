package br.com.trama.gerenciadorpedidos.views.form.produto;

import java.util.ArrayList;
import java.util.List;

import br.com.trama.gerenciadorpedidos.model.entity.Produto;
import br.com.trama.gerenciadorpedidos.views.form.FormBuilderStrategy;
import br.com.trama.gerenciadorpedidos.views.form.FormFragmentViewPagerTemplate;

public class ProdutoFormBuilderStrategy implements FormBuilderStrategy {

	private Long id;
	private Produto produto;

	public ProdutoFormBuilderStrategy(Long id) {
		super();
		this.id = id;
	}

	@Override
	public List<FormFragmentViewPagerTemplate<Produto>> getFormFragments() {

		produto = new Produto();

		if(id != null && id.intValue() > 0){
			produto = Produto.findById(Produto.class, id);
		}

		List<FormFragmentViewPagerTemplate<Produto>> list = 
				new ArrayList<FormFragmentViewPagerTemplate<Produto>>();

		ProdutoFragment produtoFragment = new ProdutoFragment();
		produtoFragment.setEntity(produto);

		list.add(produtoFragment);

		return list;
	}

	@Override
	public void save() {
		produto.save();
	}
}
