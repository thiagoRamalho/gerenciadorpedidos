package br.com.trama.gerenciadorpedidos.model.entity;

import java.io.Serializable;

import com.orm.SugarRecord;

public class Fornecedor extends SugarRecord<Fornecedor> implements Serializable{

	private static final long serialVersionUID = 1L;

	private String nome;
	
	private String email;
	
	private String linhaProdutos;
	
	private boolean isAtivo = true;

	public void setLinhaProdutos(String linhaProdutos) {
		this.linhaProdutos = linhaProdutos;
	}
	
	public String getLinhaProdutos() {
		return linhaProdutos;
	}
	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public boolean isAtivo() {
		return isAtivo;
	}

	public void setAtivo(boolean isAtivo) {
		this.isAtivo = isAtivo;
	}
	
	@Override
	public String toString() {
		return this.nome;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Fornecedor other = (Fornecedor) obj;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		return true;
	}
}
