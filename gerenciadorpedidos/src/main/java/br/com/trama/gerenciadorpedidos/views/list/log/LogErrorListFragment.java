package br.com.trama.gerenciadorpedidos.views.list.log;

import java.util.List;

import roboguice.fragment.RoboFragment;
import roboguice.inject.InjectView;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import br.com.trama.gerenciadorpedidos.R;
import br.com.trama.gerenciadorpedidos.model.QueryUtil;
import br.com.trama.gerenciadorpedidos.model.entity.StatusSendMessage;

public class LogErrorListFragment extends RoboFragment{

	private String TAG = LogErrorListFragment.class.getSimpleName();
	
	@InjectView(R.id.defaultListView) 
	protected ListView listView;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		return inflater.inflate(R.layout.default_list_layout, container, false);
	}
	
	@Override
	public void onResume() {
		
		Log.d(TAG, "onResume");
		
		if(listView!= null){
			
			List<StatusSendMessage> list = 
			StatusSendMessage.findWithQuery(StatusSendMessage.class, QueryUtil.FROM_STATUS_SEND_MESSAGE_DESC);
			
			LogErrorAdapter produtosAdapter = new LogErrorAdapter(list, getActivity());
			listView.setAdapter(produtosAdapter);
		}
		
		super.onResume();
	}
}
