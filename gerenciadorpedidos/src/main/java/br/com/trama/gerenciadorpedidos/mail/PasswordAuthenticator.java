package br.com.trama.gerenciadorpedidos.mail;

import javax.mail.PasswordAuthentication;

public class PasswordAuthenticator extends javax.mail.Authenticator{
	
	private final String userName;
	private final String password;
	
	public PasswordAuthenticator(String userName, String password) {
		super();
		this.userName = userName;
		this.password = password;
	}

	@Override
	protected PasswordAuthentication getPasswordAuthentication() {
		return new PasswordAuthentication(userName, password);
	}
}
