package br.com.trama.gerenciadorpedidos.validator;

import android.widget.TextView;
import br.com.trama.validators.AlgorithmValidator;
import br.com.trama.validators.inscricaoestadual.InscricaoEstatdualSPStrategy;

public class IEValidator {

	private final AlgorithmValidator algorithmValidator;
	
	public IEValidator() {
		this.algorithmValidator = new AlgorithmValidator(new InscricaoEstatdualSPStrategy());
	}

	public boolean isValid(TextView arg0) {
		return algorithmValidator.isValid(arg0.getText().toString());
	}
}
