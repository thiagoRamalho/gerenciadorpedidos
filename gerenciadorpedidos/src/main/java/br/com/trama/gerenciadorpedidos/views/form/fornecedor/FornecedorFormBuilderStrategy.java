package br.com.trama.gerenciadorpedidos.views.form.fornecedor;

import java.util.ArrayList;
import java.util.List;

import br.com.trama.gerenciadorpedidos.model.entity.Fornecedor;
import br.com.trama.gerenciadorpedidos.views.form.FormBuilderStrategy;
import br.com.trama.gerenciadorpedidos.views.form.FormFragmentViewPagerTemplate;

public class FornecedorFormBuilderStrategy implements FormBuilderStrategy{

	private Long id;
	private Fornecedor fornecedor;

	public FornecedorFormBuilderStrategy(Long id) {
		super();
		this.id = id;
	}

	@Override
	public List<FormFragmentViewPagerTemplate<Fornecedor>> getFormFragments() {

		fornecedor = new Fornecedor();

		if(id != null && id.intValue() > 0){
			fornecedor = Fornecedor.findById(Fornecedor.class, id);
		}

		List<FormFragmentViewPagerTemplate<Fornecedor>> list = 
				new ArrayList<FormFragmentViewPagerTemplate<Fornecedor>>();

		FornecedorFragment fornecedorFragment = new FornecedorFragment();
		fornecedorFragment.setEntity(fornecedor);

		list.add(fornecedorFragment);

		return list;
	}

	@Override
	public void save() {
		fornecedor.save();
	}
}
