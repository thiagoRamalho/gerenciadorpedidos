package br.com.trama.gerenciadorpedidos.views.list.produtos;


import java.util.List;

import android.text.TextUtils;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.View;
import br.com.trama.gerenciadorpedidos.R;
import br.com.trama.gerenciadorpedidos.model.dao.ProdutoDAO;
import br.com.trama.gerenciadorpedidos.model.entity.Carrinho;
import br.com.trama.gerenciadorpedidos.model.entity.Fornecedor;
import br.com.trama.gerenciadorpedidos.model.entity.Produto;
import br.com.trama.gerenciadorpedidos.views.form.FormManagerFragmentActivity;
import br.com.trama.gerenciadorpedidos.views.form.fornecedor.OperationType;
import br.com.trama.gerenciadorpedidos.views.form.pedido.FormPedidoFragmentActivity;
import br.com.trama.gerenciadorpedidos.views.list.CustomSearchViewListFragment;
import br.com.trama.gerenciadorpedidos.views.list.SearchStrategy;
import br.com.trama.gerenciadorpedidos.views.list.fornecedor.FornecedorManager;

import com.actionbarsherlock.view.MenuItem;
import com.google.inject.Inject;

public class ProdutosListFragment extends CustomSearchViewListFragment<Produto>{

	private final String TAG = ProdutosListFragment.class.getSimpleName();

	@Inject
	private FornecedorManager fornecedorManager;
	
	@Inject
	private ProdutoDAO produtoDAO;

	private SearchStrategy<Produto> searchStrategy;
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		Log.d(TAG, "onOptionsItemSelected ["+item.getItemId()+"]");

		if(item.getItemId() == R.id.add_id_menu_action_bar){
			
			if(this.fornecedorManager.possui(getActivity())) {
				runTo(FormManagerFragmentActivity.class, Produto.class, null);
			}
		}

		return true;
	}


	@Override
	public void onResume() {
		
		Log.d(TAG, "onResume");
		
		searchStrategy = new SearchAll(produtoDAO);
		
		if(!OperationType.ALL.equals(operationType)){
			Fornecedor fornecedor = Carrinho.getPedido().getFornecedor();
			searchStrategy = new SearchFornecedor(produtoDAO, fornecedor);
		}
		
		setData(searchStrategy.search());
		
		super.onResume();
	}

	private void setData(List<Produto> findAll) {

		if(listView!= null){
			ProdutosAdapter produtosAdapter = new ProdutosAdapter(findAll, getActivity());
			listView.setAdapter(produtosAdapter);
		}
	}
	
	@Override
	public boolean onMenuItemActionCollapse(MenuItem item) {
		
		Log.d(TAG, "onMenuItemActionCollapse");
		
		mSearchView.setQuery("", false);
		
		setData(searchStrategy.search());
		
		return true;
	}

	
	@Override
	public boolean onQueryTextChange(String text) {
		
		boolean isChange = false;
		
		Log.d(TAG, "onQueryTextChange ["+text+"]");

		if(!TextUtils.isEmpty(text)){
			
			setData(this.searchStrategy.searchBy(text));
			
			isChange = true;
		}

		return isChange;
	}

	@Override
	public boolean onQueryTextSubmit(String text) {
		
		Log.d(TAG, "onQueryTextSubmit ["+text+"]");
		
		return this.onQueryTextChange(text);
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {

		getActivity().getMenuInflater().inflate(R.menu.context_menu, menu);

		android.view.MenuItem add  = menu.findItem(R.id.add_context_menu_id);
		android.view.MenuItem edit = menu.findItem(R.id.edit_context_menu_id);
		
		if(!super.entity.isAtivo() || OperationType.ALL.equals(operationType)){
			menu.removeItem(add.getItemId());
		}
		
		if(OperationType.ADD_PRODUTO_PEDIDO.equals(operationType)){
			menu.removeItem(edit.getItemId());
		}
	}

	@Override
	public boolean onContextItemSelected(android.view.MenuItem item) {

		Log.d(TAG, "onContextItemSelected ["+item.getItemId()+"]");

		switch (item.getItemId()) {
		case R.id.edit_context_menu_id:
			runTo(FormManagerFragmentActivity.class, Produto.class, this.entity.getId());
			break;

		case R.id.add_context_menu_id:
			Carrinho.addItem(this.entity);
			runTo(FormPedidoFragmentActivity.class, Produto.class, this.entity.getId());
			break;

		default:
			break;
		}

		return true;
	}
}
