package br.com.trama.gerenciadorpedidos.model;

import java.util.Locale;

public abstract class QueryUtil {

	private static final String AND = " AND ";
	
	public static final String ORDER_BY_ID_DESC = " ORDER BY ID DESC";
	public static final String ORDER__RAZAO_SOCIAL_ASC = " ORDER BY RAZAO_SOCIAL ASC";
	
	public static final String BY_RAZAO_SOCIAL = " RAZAO_SOCIAL LIKE  UPPER(?) ";
	public static final String BY_NOME_FANTASIA = " NOME_FANTASIA LIKE  UPPER(?) ";
	public static final String BY_CNPJ = " CNPJ = ? ";
	public static final String BY_IE = " INSCRICAO_ESTADUAL = ? ";
	public static final String BY_IM = " INSCRICAO_MUNICIPAL = ? ";
	
	public static final String BY_ATIVO = " IS_ATIVO = ?";
	
	public static final String BY_NOME = " NOME LIKE  UPPER(?) ";
	
	public static final String BY_FORNECEDOR = " FORNECEDOR = ? ";
	
	public static final String BY_ATIVO_AND_FORNECEDOR =  BY_ATIVO + AND + BY_FORNECEDOR;
	
	public static final String BY_NOME_AND_ATIVO = BY_NOME + AND + BY_ATIVO;

	public static final String  BY_NOME_AND_FORNECEDOR = BY_NOME + AND + BY_FORNECEDOR;
	
	public static final String BY_ID = " ID = ? ";
	
	public static final String BY_CODIGO = " CODIGO = ?";
	
	public static final String FROM_STATUS_SEND_MESSAGE_DESC = "SELECT * FROM STATUS_SEND_MESSAGE"+ORDER_BY_ID_DESC;
	
	public static final String FROM_PEDIDO_DESC = "SELECT * FROM PEDIDO"+ORDER_BY_ID_DESC;
	
	public static final String BY_SYNC_STATUS = " IS_SINCRONIZADO = ? ";
	
	public static final String WHERE_ID =" WHERE ID = ? ";
	
	public static final String UPDATE_PEDIDO_SYNC_STATUS_BY_ID = "UPDATE PEDIDO SET"+BY_SYNC_STATUS+WHERE_ID;
	
	public static String like(String text) {
		return '%' + text.trim().toUpperCase(Locale.getDefault()) + '%';
	}
}
