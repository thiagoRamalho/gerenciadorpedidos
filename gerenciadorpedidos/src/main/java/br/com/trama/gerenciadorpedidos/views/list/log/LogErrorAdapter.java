package br.com.trama.gerenciadorpedidos.views.list.log;

import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import br.com.trama.gerenciadorpedidos.R;
import br.com.trama.gerenciadorpedidos.model.entity.StatusSendMessage;
import br.com.trama.gerenciadorpedidos.util.ComponentUtil;

@SuppressLint("ViewTag")
public class LogErrorAdapter extends BaseAdapter{

	private List<StatusSendMessage> logs;
	private LayoutInflater inflater;
	private ComponentUtil componentUtil;
	
	public LogErrorAdapter(List<StatusSendMessage> logs, Context context) {
		super();
		this.logs = logs;
		this.inflater = LayoutInflater.from(context);
		this.componentUtil = new ComponentUtil();
	}

	@Override
	public int getCount() {
		return this.logs.size();
	}

	@Override
	public StatusSendMessage getItem(int position) {
		return this.logs.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		
		TextView smtpAddress, smtpPort, smtpSubject, exceMessage, date;
		
		if (convertView == null) {
			
			convertView = inflater.inflate(R.layout.row_log_layout, parent, false);
			
			smtpAddress = (TextView)  convertView.findViewById(R.id.log_smtp_address_id);
			smtpPort 	= (TextView)  convertView.findViewById(R.id.log_smtp_port_id);
			smtpSubject = (TextView)  convertView.findViewById(R.id.log_smtp_subject_id);
			exceMessage = (TextView)  convertView.findViewById(R.id.log_exception_msg_id);
			date        = (TextView)  convertView.findViewById(R.id.log_date_id);
			
			convertView.setTag(R.id.log_smtp_address_id, smtpAddress);
			convertView.setTag(R.id.log_smtp_port_id, smtpPort);
			convertView.setTag(R.id.log_smtp_subject_id, smtpSubject);
			convertView.setTag(R.id.log_exception_msg_id, exceMessage);
			convertView.setTag(R.id.log_date_id, date);
		
		} else {
			smtpAddress = (TextView)  convertView.getTag(R.id.log_smtp_address_id);
			smtpPort 	= (TextView)  convertView.getTag(R.id.log_smtp_port_id);
			smtpSubject = (TextView)  convertView.getTag(R.id.log_smtp_subject_id);
			exceMessage = (TextView)  convertView.getTag(R.id.log_exception_msg_id);
			date		= (TextView)  convertView.getTag(R.id.log_date_id);
		}
		
		StatusSendMessage entidade = this.getItem(position);
		
		smtpAddress.setText(entidade.getSmtp());
		
		smtpPort.setText(""+entidade.getPort());
		
		smtpSubject.setText(entidade.getSubject());
		
		exceMessage.setText(entidade.getEx());
		
		date.setText(this.componentUtil.calendarToString(entidade.getDate()));
		
		return convertView;
	}
}
