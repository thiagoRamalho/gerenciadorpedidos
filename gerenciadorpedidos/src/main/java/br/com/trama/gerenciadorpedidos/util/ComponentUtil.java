package br.com.trama.gerenciadorpedidos.util;

import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import android.text.TextUtils;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

public class ComponentUtil {

	public String extractValue(TextView component, String... replaceRegex) {
		
		String text = component.getText().toString();
		text = text.trim();
		
		if(replaceRegex.length > 0){
			text = text.replaceAll(replaceRegex[0], "");
		}
		
		return text;
	}

	public String extractValue(Spinner component, String... replaceRegex) {
		
		String text = component.getSelectedItem().toString();
		text = text.trim();
		
		if(replaceRegex.length > 0){
			text = text.replaceAll(replaceRegex[0], "");
		}
		
		return text;
	}
	
	public boolean isEmpty(EditText component, String... regexRawValue) {
		
		String text = this.extractValue(component, regexRawValue);
		
		return TextUtils.isEmpty(text);
	}

	public boolean containsMin(EditText component, int min, String... regexRawValue){
		
		boolean contains = false;
		
		if(!this.isEmpty(component, regexRawValue)){
			String text = this.extractValue(component, regexRawValue);
			contains = text.length() >= min;
		}
		
		return contains;
	}

	public boolean validateMobilePhone(EditText mobilePhone, boolean isRequired) {

		return this.validatePhone(mobilePhone, isRequired, ConstantsUtil.SIZE_11, ConstantsUtil.DENIEL_NUMBERS_REGEX);
	}

	public boolean validatePhone(EditText phone, boolean isRequired, int lenghtDigits, String... regexRawValue) {
		
		boolean isOk = true;
		
		if(isRequired || !this.isEmpty(phone, regexRawValue)){
			isOk = this.containsMin(phone, lenghtDigits, regexRawValue);
		}
		
		return isOk;
	}

	public boolean validateEmail(EditText email, boolean isRequired) {
		
		boolean isOk = true;
		
		String extractValue = this.extractValue(email);
		
		if(isRequired || !TextUtils.isEmpty(extractValue)){
			isOk = android.util.Patterns.EMAIL_ADDRESS.matcher(extractValue).matches();
		}
		
		return isOk;
	}

	public String doubleToCurrency(double doubleValue) {
		
		NumberFormat format = NumberFormat.getCurrencyInstance();
		
		return format.format(doubleValue);
	}

	public double currencyToDouble(String stringValue) {

		double preco = 0;

		try {

			NumberFormat currencyInstance = NumberFormat.getCurrencyInstance(Locale.getDefault());
			Number parse = currencyInstance.parse(stringValue);
			preco = parse.doubleValue();

		} catch (ParseException e) {
			preco = 0;
		}

		return preco;
	}

	public CharSequence calendarToString(Calendar date) {
		
		java.text.DateFormat dateTimeInstance = SimpleDateFormat.getDateTimeInstance();
		
		return dateTimeInstance.format(date.getTime());
	}
}
