package br.com.trama.gerenciadorpedidos.model.entity;



public abstract class Carrinho {

	private static Pedido pedido;
	private static int currentTab = 0;

	public static void clean(){
		pedido = new Pedido();
		currentTab = 0;
	}

	public static void addCliente(Cliente cliente) {
		getPedido().setCliente(cliente);
	}

	public static Pedido getPedido() {

		if(pedido == null){
			pedido = new Pedido();
		}

		return pedido;
	}

	public static void addItem(Produto entity) {
		
		double preco = pedido.isAVista() ? entity.getPrecoUnidadeVista() : entity.getPrecoUnidadePrazo();
		
		ItemPedido itemPedido = new ItemPedido(pedido, entity.getId(), preco, 1);
		itemPedido.setNome(entity.getNome());
		
		getPedido().addItem(itemPedido);
	}

	public static void setPedido(Pedido p) {
		pedido = p;
	}

	public static void addFornecedor(Fornecedor entity) {

		Pedido pedido = getPedido();

		Fornecedor fornecedorActual = pedido.getFornecedor();
		
		if(fornecedorActual != null && !fornecedorActual.equals(entity)){
			pedido.cleanItens();
		}

		pedido.setFornecedor(entity);
	}

	public static int getCurrentTab() {
		return currentTab;
	}
	
	public static void setCurrentTab(int index){
		currentTab = index;
	}
}
