package br.com.trama.gerenciadorpedidos.mail;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import android.app.IntentService;
import android.content.Intent;
import android.os.SystemClock;
import android.util.Log;
import br.com.trama.gerenciadorpedidos.model.QueryUtil;
import br.com.trama.gerenciadorpedidos.model.entity.Cliente;
import br.com.trama.gerenciadorpedidos.model.entity.Comunicacao;
import br.com.trama.gerenciadorpedidos.model.entity.Fornecedor;
import br.com.trama.gerenciadorpedidos.model.entity.Pedido;
import br.com.trama.gerenciadorpedidos.model.entity.StatusSendMessage;
import br.com.trama.gerenciadorpedidos.util.Connection;
import br.com.trama.gerenciadorpedidos.util.ConstantsUtil;
import br.com.trama.gerenciadorpedidos.views.form.settings.Settings;

public class SendMailIntentService extends IntentService{

	private static final String TAG = SendMailIntentService.class.getSimpleName();
	private Settings settings;
	private SendMailJavaMailBuilder sendMailBuilder;

	public SendMailIntentService() {
		super(TAG);
	}

	@Override
	protected void onHandleIntent(Intent intent) {

		Log.d(TAG, "Iniciando Processo de envio de Pedidos");

		int countSend = 0;

		settings = new Settings(getApplicationContext());

		if(settings.isSendEmail() && Connection.isConnection(getApplicationContext())){

			sendMailBuilder = 
					new SendMailJavaMailBuilder(this.settings.getSMTP(), this.settings.getPort(), getApplicationContext());

			sendMailBuilder
			.auth(this.settings.getUserSMTP(), this.settings.getUserPassSMTP())
			.from(this.settings.getUserSMTP());

			List<Pedido> find = Pedido.find(Pedido.class, QueryUtil.BY_SYNC_STATUS, new String[]{"0"});

			for (Pedido pedido : find) {
				
				pedido.sincItens();
				
				try {

					if(sendEmail(pedido)){

						Pedido.executeQuery(QueryUtil.UPDATE_PEDIDO_SYNC_STATUS_BY_ID, new String[]{"1", pedido.getId().toString()});
						countSend++;
					} 

					SystemClock.sleep(ConstantsUtil.ONE_SECONDS_IN_MILIS);

				} catch (Exception e) {
					Log.d(TAG, ""+e.getMessage());
				}
			}

			StatusSendMessage.saveInTx(sendMailBuilder.getStatusSendMessage());
		}
		
		sendBroadcast(countSend);
		
		Log.d(TAG, "Finalizando Processo de envio de Pedidos");
	}

	private void sendBroadcast(int count) {
		Intent mailBroadCast = new Intent();
		mailBroadCast.setAction(ConstantsUtil.BROADCAST_SEND_MAIL);
		mailBroadCast.putExtra(ConstantsUtil.TOTAL_SEND_MAIL, count);
		sendBroadcast(mailBroadCast);
	}

	public boolean sendEmail(Pedido pedido) throws IOException {

		boolean isOk = false;

		Cliente cliente = pedido.getCliente();
		Comunicacao comunicacao = cliente.getComunicacao();

		Fornecedor fornecedor = pedido.getFornecedor();

		String subject = createSubject(pedido);
		String attachContent = createAttachContent(pedido);

		File file = createFile(subject, attachContent);

		try{

			if(this.settings.isSendEmailFornecedor()){
				sendMailBuilder.to(fornecedor.getEmail());
			}
			if(this.settings.isSendEmailCliente()){
				sendMailBuilder.cc(comunicacao.getEmail());
			}
			sendMailBuilder.subject(subject)
			.withMessageBody(getMessageBody(pedido.getCliente(), settings.getVendedor()))
			.withAttach(file);

			isOk = sendMailBuilder.send();

		}finally{
			file.delete();
		}

		return isOk;
	}

	private String getMessageBody(Cliente cliente, String vendedor) {

		StringBuilder builder = new StringBuilder();
		builder
		.append("Segue pedido cliente ")
		.append(cliente.getNomeFantasia())
		.append(System.getProperty("line.separator"))
		.append(System.getProperty("line.separator"))
		.append("Atenciosamente,")
		.append(System.getProperty("line.separator"))
		.append(System.getProperty("line.separator"))
		.append(vendedor)
		.append(System.getProperty("line.separator"))
		.append(System.getProperty("line.separator"));

		return builder.toString();
	}

	private File createFile(String subject, String messageBody) {

		File file = null;

		try {

			StringBuilder builder = new StringBuilder();
			builder
			.append(subject.replaceAll("[[^a-zA-Z0-9]]", "_"));

			file = File.createTempFile(builder.toString(), ".csv", getApplicationContext().getCacheDir());

			BufferedWriter output = new BufferedWriter(new FileWriter(file));
			output.write(messageBody);
			output.close();

		} catch ( IOException e ) {
			Log.d(TAG, e.getMessage());
			throw new RuntimeException(e);
		}

		return file;
	}

	private String createAttachContent(Pedido pedido) {

		TemplateMessage templateMessage = new TemplateMessage(pedido, settings.getVendedor(), getBaseContext());
		
		return templateMessage.create();
	}

	private String createSubject(Pedido pedido) {

		Date dateTime = Calendar.getInstance().getTime();

		java.text.DateFormat dateTimeInstance = 
				SimpleDateFormat.getDateTimeInstance(SimpleDateFormat.SHORT, SimpleDateFormat.SHORT);

		StringBuilder builder = new StringBuilder("Pedido");
		builder.append(" ["+pedido.getId().toString()+"] ")
		.append("Cliente")
		.append(" ["+pedido.getCliente().getNomeFantasia()+"] ")
		.append(" ["+dateTimeInstance.format(dateTime)+"]");

		return builder.toString();
	}
}
