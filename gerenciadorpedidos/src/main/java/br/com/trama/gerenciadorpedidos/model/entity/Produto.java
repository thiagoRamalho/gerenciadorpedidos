package br.com.trama.gerenciadorpedidos.model.entity;

import java.io.Serializable;

import com.orm.SugarRecord;

public class Produto extends SugarRecord<Produto> implements Serializable{

	private static final long serialVersionUID = 1L;

	String nome;

	double precoUnidadeVista;
	
	double precoUnidadePrazo;

	String codigo;

	private boolean isAtivo = true;

	private Fornecedor fornecedor;

	public Fornecedor getFornecedor() {
		return fornecedor;
	}

	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public double getPrecoUnidadeVista() {
		return precoUnidadeVista;
	}

	public void setPrecoUnidadeVista(double precoUnidadeVista) {
		this.precoUnidadeVista = precoUnidadeVista;
	}
	
	public void setPrecoUnidadePrazo(double precoUnidadePrazo) {
		this.precoUnidadePrazo = precoUnidadePrazo;
	}
	
	public double getPrecoUnidadePrazo() {
		return precoUnidadePrazo;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public void setAtivo(boolean isAtivo) {
		this.isAtivo = isAtivo;
	}

	public boolean isAtivo() {
		return isAtivo;
	}

	@Override
	public String toString() {
		return "Produto [nome=" + nome + ", precoUnidadeVista="
				+ precoUnidadeVista + ", precoUnidadePrazo="
				+ precoUnidadePrazo + ", codigo=" + codigo + ", isAtivo="
				+ isAtivo + ", fornecedor=" + fornecedor + "]";
	}
}
