package br.com.trama.gerenciadorpedidos.listeners;

import br.com.trama.gerenciadorpedidos.R;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class PreviousClickListener implements OnClickListener {

	private ViewPager viewPager;
	private Button btnNext;
	private Button btnPrevious;
	private PagerAdapter pageAdapter;
	
	public PreviousClickListener(ViewPager viewPager, Button btnNext,
			Button btnPrevious, PagerAdapter pageAdapter) {
		super();
		this.viewPager = viewPager;
		this.btnNext = btnNext;
		this.btnPrevious = btnPrevious;
		this.pageAdapter = pageAdapter;
		
		this.init();
	}


	private void init(){
		
		int maxPage = pageAdapter.getCount() -1;
		int currentItem = viewPager.getCurrentItem();

		if(currentItem > 0){
			currentItem--;
		}
		
		btnPrevious.setEnabled(currentItem > 0);
		
		if(currentItem < maxPage){
			btnNext.setText(R.string.next);
		}
		
		viewPager.setCurrentItem(currentItem);
	}

	
	@Override
	public void onClick(View v) {
		
		this.init();
	}

}
