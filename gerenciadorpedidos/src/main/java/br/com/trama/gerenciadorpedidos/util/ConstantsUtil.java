package br.com.trama.gerenciadorpedidos.util;


public abstract class ConstantsUtil {
	
	public static final String PROP_MAIL_FILE = "prop_mail_file.properties";
	
	public static final String KEY_PROP_MAIL_SMTP_URL = "mail.smtp.host";
	public static final String KEY_PROP_MAIL_SMTP_PORT = "mail.smtp.port";
	
	public static final String ITEM_KEY = "ITEM_KEY";
	public static final String ENTITY_TYPE = "ENTITY_TYPE";
	public static final String ENTITY_ID = "ENTITY_ID";
	public static final String CACHE_ENTITY = "CACHE_ENTITY";
	
	public static final String DENIEL_NUMBERS_REGEX = "[[^0-9]]";
	public static final String DENIEL_NUMBERS_DOT_REGEX = "[[^0-9.,]]";

	public static final String MASK_CNPJ = "##.###.###/####-##";
	public static final String MASK_IE = "###.###.###.###";
	public static final String MASK_CEP = "#####-###";
	public static final String MASK_TEL = "(##)####-####";
	public static final String MASK_CEL = "(##)#####-####";

	
	public static final int SIZE_8 = 8;
	public static final int SIZE_11 = 11;
	public static final int SIZE_10 = 10;
	
	public static final double MIN_VALUE_NUMBER = 0.01;
	
	public static final int INDEX_NAV_PEDIDO  = 0;
	public static final int INDEX_NAV_CLIENTE = 1;
	public static final int INDEX_NAV_FORNECE = 2;
	public static final int INDEX_NAV_PRODUTO = 3;
	public static final int INDEX_NAV_ROTEIRO = 4;
	public static final int INDEX_NAV_LOG     = 5;
	public static final int INDEX_NAV_CONFIG  = 6;
	
	public static final String TITLE_FRAGMENT = "TITLE_FRAGMENT";
	public static final String SUBTITLE_FRAGMENT = "SUBTITLE_FRAGMENT";
	public static final String ACTION_EDIT = "ACTION_EDIT";
	
	public static final int ONE_SECONDS_IN_MILIS = 1000;

	public static final String BROADCAST_SEND_MAIL = ConstantsUtil.class.getPackage()+".BROADCAST_SEND_MAIL";

	public static final String TOTAL_SEND_MAIL = "TOTAL_SEND_MAIL";

	public static final String FORNECEDOR_KEY = "FORNECEDOR_KEY";


}
