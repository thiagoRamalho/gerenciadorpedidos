package br.com.trama.gerenciadorpedidos.views.form;

import java.util.List;

import roboguice.inject.ContentView;
import roboguice.inject.InjectView;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import br.com.trama.gerenciadorpedidos.R;
import br.com.trama.gerenciadorpedidos.listeners.PreviousClickListener;
import br.com.trama.gerenciadorpedidos.util.ConstantsUtil;

import com.github.rtyley.android.sherlock.roboguice.activity.RoboSherlockFragmentActivity;
import com.google.inject.Inject;

@ContentView(R.layout.form_layout)
public class FormManagerFragmentActivity extends RoboSherlockFragmentActivity implements OnClickListener{

	private final String TAG = FormManagerFragmentActivity.class.getSimpleName();

	@InjectView(R.id.viewpager) 
	private ViewPagerManager viewPager;

	private FormPageAdapter pageAdapter;

	@Inject
	private FormPageBuilder formPageBuilder;

	@InjectView(R.id.btn_id_form_previous) 
	private Button btnPrevious;

	@InjectView(R.id.btn_id_form_next) 
	private Button btnNext;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);

		FormBuilderStrategy strategy = configStrategy();

		configViewPager(strategy.getFormFragments());

		this.initButtons();

		Log.d(TAG,"onCreate");
	}

	private void configViewPager(List<? extends FormFragmentViewPagerTemplate<?>> formFragment) {

		pageAdapter = new FormPageAdapter(getSupportFragmentManager());

		pageAdapter.set(formFragment);

		viewPager.setOffscreenPageLimit(formFragment.size());

		viewPager.setAdapter(pageAdapter);

		this.viewPager.setTouchScroll(false);
	}

	private FormBuilderStrategy configStrategy() {

		Intent intent = getIntent();

		Class<?> entityType = (Class<?>) intent.getSerializableExtra(ConstantsUtil.ENTITY_TYPE);

		Long id = (Long) intent.getSerializableExtra(ConstantsUtil.ENTITY_ID);

		FormBuilderStrategy strategy = 
				formPageBuilder
				.withId(id)
				.withType(entityType)
				.create();

		return strategy;
	}

	private void initButtons() {

		PreviousClickListener listener = new PreviousClickListener(viewPager, btnNext, btnPrevious, pageAdapter);
		
		this.btnPrevious.setOnClickListener(listener);

		this.btnNext.setOnClickListener(this);

		int maxPage = pageAdapter.getCount() -1;
		int currentItem = viewPager.getCurrentItem();

		btnPrevious.setEnabled(currentItem > 0);

		if(currentItem == maxPage){
			btnNext.setText(R.string.finish);
		}
	}

	@Override
	protected void onStart() {

		Log.d(TAG,"onStart");

		super.onStart();
	}

	@Override
	public void onClick(View v) {

		int currentItem = viewPager.getCurrentItem();

		FormFragmentViewPagerTemplate<?> item = pageAdapter.getItem(currentItem);

		if(item.next()){

			int maxPage = pageAdapter.getCount();

			currentItem++;

			if(currentItem > maxPage || getString(R.string.finish).equals(btnNext.getText())){
				formPageBuilder.save();
				finish();
			} 
			else {

				if(currentItem >= maxPage - 1){
					btnNext.setText(R.string.finish);
				}

				if(currentItem < maxPage){
					viewPager.setCurrentItem(currentItem);
				}
			}
		}

		btnPrevious.setEnabled(currentItem > 0);
	}
}
